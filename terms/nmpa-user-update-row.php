<?php

include "../config/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;

$tbname = "_xd_nmpa_user_list";

$action = $_POST['action'];
$xlauditlog=$_POST['xlauditlog'];

//for row update operation;
if ($action == 'row-update') {
	$uid = $_POST['uid'];
	$username_cn = $_POST['username_cn'];
	$address = $_POST['address'];
    $phone = $_POST['phone'];
    $alias = $_POST['alias'];
	$cellphone = $_POST['cellphone'];
    $contact = $_POST['contact'];
    $contact_phone = $_POST['contact_phone'];
    $xltype = $_POST['xltype'];
    $xlstat = $_POST['xlstat'];
    $xlmouser = strtoupper($_POST['userid']);
	$xlmodtc = RUN_DTC;
    $xlcomment = $_POST['xlcomment'];
    $xlauditlog = $_POST['xlauditlog'];
    $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated ';

    $sql = "UPDATE " . $tbname . " 
            SET `username_cn` = '$username_cn', `alias` = '$alias', `phone` = '$phone', `cellphone` = '$cellphone', `contact` = '$contact',
                `contact_phone` = '$contact_phone', `xlstat` = '$xlstat', `xltype` = '$xltype',
                `xlmouser` = '$xlmouser', `xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog',
                `xlcomment` = '$xlcomment'
            WHERE `uid` = '$uid' ";
	
	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "User profile updated successfully";
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "User profile updated failed";
		$res['debug'] = $sql;
        $res['auditlog']=$xlauditlog;
	}
}

$res['debug'] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>