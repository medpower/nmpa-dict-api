<?php

require_once "../config/_init_.php";

cors();

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");
$tbname="_xd_nmpa_study_variable_label";

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'query';
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where xlrmfl <> 'Y' ";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$key =explode("-","xltest-xltestcd");

//$keyvalue = urldecode($_POST['keyvalue']);
$keyvalue = urldecode($_GET['term']);

$key_where_condition = "0>1";
foreach ($key as $keyname){	
	$key_where_condition = $key_where_condition . " or " . $keyname . " like '" . $keyvalue . "'";
};


$where_condition= $where_condition . " and (" . $key_where_condition . ")";


//For category fetching purpose;

//For read operation;
if ($action == 'query') {

	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query("SELECT xlmodify FROM " . $tbname . " utf8 " . $where_condition . " ");
	$records = array();
	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}
	$res['records'] = $records;
	$res['message'] = "Query successfully";
	//$res['debug'] = "SELECT * FROM " . $tbname . " utf8 " . $where_condition . " ";
}



//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>