<?php

include "./_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Reset the action when applicable;

if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$tbname = '_xd_nmpa_study_info';

$where_condition="where 1 > 0 ";

//for cell update operation;
if ($action == 'row-update') {

    $id = trim($_POST['id']);
    $userid = strtoupper(trim($_POST['userid']));
    $protocolid = trim($_POST['protocolid']);
	$studyid = trim($_POST['studyid']);
	$studytitle = trim($_POST['studytitle']);
    $indication = trim($_POST['indication']);
    $taname = trim($_POST['taname']);

    $xledctype = trim($_POST['xledctype']);
    $xldttype = trim($_POST['xldttype']);
    $xlspectype = trim($_POST['xlspectype']);
    $xlcrftype = trim($_POST['xlcrftype']);

	$xldopoc = trim($_POST['xldopoc']);
	$xlrapoc = trim($_POST['xlrapoc']);
	$xlsppoc = trim($_POST['xlsppoc']);
	$xldmpoc = trim($_POST['xldmpoc']);
    $xlcdpoc = trim($_POST['xlcdpoc']);
    $xlmwpoc = trim($_POST['xlmwpoc']);

    $xlendtc = $_POST['xlendtc1'];
    $xldpdtcp =$_POST['xldpdtcp1'];
    $xltcdtcp =$_POST['xltcdtcp1'];
    $xldadtc = $_POST['xldadtc1'];

	$xlstat = "ACTIVE";
	$xlfl = "N";
    $xlcruserid = trim($_POST['userid']);
    $xlmouserid = trim($_POST['userid']);
	$xlmodtc = RUN_DTC;
	$xlcomment = trim($_POST['xlcomment']);
	
    $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $userid . ' updated';

    $sql = "UPDATE " . $tbname . " 
    SET `protocolid` = '$protocolid', `studyid` = '$studyid', `studytitle` = '$studytitle',
        `indication` = '$indication',`taname` = '$taname',
        `xlendtc` = '$xlendtc',`xldpdtcp` = '$xldpdtcp',`xltcdtcp` = '$xltcdtcp',`xldadtc` = '$xldadtc',
        `xldopoc` = '$xldopoc',`xlrapoc` = '$xlrapoc',`xlsppoc` = '$xlsppoc',`xldmpoc` = '$xldmpoc',`xlcdpoc` = '$xlcdpoc',`xlmwpoc` = '$xlmwpoc',
        `xledctype` = '$xledctype', `xlspectype` = '$xlspectype', `xldttype` = '$xldttype', `xlcrftype` = '$xlcrftype',
        `xlmouserid` = '$userid', `xlmodtc` = '$xlmodtc', `xlcomment` = '$xlcomment',
        `xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$xlauditlog')
    WHERE `id` = '$id' 
    ";

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "项目信息更新成功！";
        // $res['debug'] = $sql;
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "项目信息更新失败！";
		// $res['debug'] = $sql;
        $res['auditlog']=$xlauditlog;
	}
}

// $res['debug'] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>