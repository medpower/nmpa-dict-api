<?php

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'filtereditems';
$xltype = "";
$testcode = "";
$status = "";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = strtolower($_POST['userid']);
}
else{
    $userid = "public";
}


if (isset($_POST['keylist']) && !empty($_POST['keylist'])) {
	$keylist=urldecode($_POST['keylist']);
    $where_condition= " a.id in (" . $keylist .  ")";
}
else{
    $keylist = "";
    $where_condition= "1 > 0";
}

                
$rnd =random_character($length = 8);
$run_dt=RUN_DTC;

// $keyscope = "global";
// $tbname="_xd_nmpa_" . $keyscope . "_" . $keycategory . "_" . $keytype;

$tbname_dict="_xd_nmpa_global_" . $keycategory . "_" . $keytype;

// $tbname_dict="_xd_nmpa_global_terms_library";

// $start=1;
// $end=100;

// $result = $conn->multi_query("update " . $tbname . " a, " . $tbname_dict . " b " . "  
//             SET a.xlmodtc = '$run_dt',
//                 a.xlmodify = b.xlmodify,
//                 a.xlstat = 'LOCKED',
//                 a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid dictionary translated')
//                 where a.xltest=b.xltest and a.id in (select * from (select id from " . $tbname . " limit " . $start . "," . $end . ") c );"
//                 );

$sql = "update " . $tbname . " a, " . $tbname_dict . " b " . "  
SET a.xlmodtc = '$run_dt',
    a.xlmodify = b.xlmodify,
    a.xlstat = 'LOCKED',
    a.xleval = 'DICT',
    a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid batch translated with global dictionary')
    where a.xltest=b.xltest and a.xlstat not in ('APPROVED', 'LOCKED') and b.xlstat in ('APPROVED', 'LOCKED') and " . $where_condition . ";";

$result = $conn->multi_query($sql);

$no=mysqli_affected_rows($conn);

// $res['sql'] = $sql;

if($result){
    $res['message'] = "字典库成功匹配翻译 " . $no . " 条目！";
    $res['debug'] = 'NA';
}else{
    $res['message'] = "字典库成功匹配翻译 0 条目！";
    $res['debug'] = $sql;
}


// if ($result) {
//     $res['message'] = "Dictionary batch translated successfully";
//     $res['debug'] = "";
// }else{
//         $res['error'] = true;
//         $res['message'] = "Dictionary batch translated failed";
//         $res['debug'] = "update " . $tbname . " a, " . $tbname_dict . " b " . "  
//         SET a.xlmodtc = '$run_dt',
//             a.xlmodify = b.xlmodify,
//             a.xlstat = 'LOCKED',
//             a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid dictionary translated')
//             where a.xltest=b.xltest and a.id in (" . $keylist .  ");";
//     };

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>