<?php

include "../config/ndp/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
//Only change the following section parameters per specific project;
$action = $_POST['action'];
$debug = 'N';
$key_type = 'N'; //N for numeric key, C for character key;
$column_type = 'C'; //N for numeric column, C for character column;

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}

$column_name = urldecode($_POST['column_name']);
$column_value= addslashes(urldecode($_POST['column_value']));
$key_name = urldecode($_POST['key_name']);
$key_value = urldecode($_POST['key_value']);



$where_condition="where 1>0 ";

if ($key_type=='N'){
    $where_condition = $where_condition . " and " . $key_name . " = " . $key_value;
}
else{
    $where_condition = $where_condition . " and " . $key_name . " = '" . $key_value . "'";
}


//Please do not change the codes below; if you are not sure how it works;
//for cell update operation;
if ($action == 'cell-update') {
	$id = $key_value;	
	$modtc  = RUN_DTC;
    $userid=$_POST['userid'];
	$auditlog = RUN_DTC . " " . $_POST['userid'] . ' updated [' .$column_name . '] to ' . $column_value ;

    if ($column_type=='C'){
        $sql="UPDATE " . $tbname . " 
        SET " . $column_name . " = '$column_value', modtc = '$modtc', mouserid = '$userid',
        auditlog = concat_ws('\\n', ifnull(auditlog,''),'$auditlog')
        " . $where_condition;
    }
    else{
        $sql="UPDATE " . $tbname . " 
        SET " . $column_name . " = " . $column_value . ", modtc = '$modtc', mouserid = '$userid',
        auditlog = concat_ws('\\n', ifnull(auditlog,''),'$auditlog')
        " . $where_condition;
    }

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "Cell saved";

        if ($debug=='Y'){
            $res['sql']=$sql;
            $res['auditlog']=$auditlog;
        }
		
		
	} else{
		$res['error'] = true;
		$res['message'] = "Cell saved failed";
		$res['debug'] = "Not applicable";
		// $res['auditlog']=$xlauditlog;
        if ($debug=='Y'){
            $res['sql']=$sql;
        }
	}
}

if ($action == 'vote-update') {
	$id = $key_value;	
	$modtc  = RUN_DTC;
    $userid=$_POST['userid'];
	$auditlog = RUN_DTC . " " . $_POST['userid'] . ' updated [' .$column_name . '] to ' . $column_value ;

    if ($column_type=='C'){
        $sql="UPDATE " . $tbname . " 
        SET " . $column_name . " = concat_ws(',', ifnull(" . $column_name . ",''),'$column_value'), modtc = '$modtc', mouserid = '$userid',
        auditlog = concat_ws('\\n', ifnull(auditlog,''),'$auditlog')
        " . $where_condition;
    }

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "Cell saved";

        if ($debug=='Y'){
            $res['sql']=$sql;
            $res['auditlog']=$auditlog;
        }
		
		
	} else{
		$res['error'] = true;
		$res['message'] = "Cell saved failed";
		$res['debug'] = "Not applicable";
		// $res['auditlog']=$xlauditlog;
        if ($debug=='Y'){
            $res['sql']=$sql;
        }
	}
}

$res['debug'] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>