<?php

include_once "../config/_init_.php";
//use \Firebase\JWT\JWT;

cors();

//get_post_args();
	
if (isset($_POST['upassword']) && !empty($_POST['upassword'])) {
	$userpassword1=urldecode($_POST['upassword']);
	$userpassword=md5(strtoupper($userid) . $userpassword1);
	$res["pw"]=$userpassword1;
}
else{
	$res["error"]=true;
	$res["message"]="Authorized failed, invalid user id or password";
	exit(json_encode($res,JSON_UNESCAPED_UNICODE));	
}

//use ldap to authorize the access when available

function ldap($userid, $userpassword) {

	header('content-type:application:json;charset=utf8');
	header('Access-Control-Allow-Origin:*');
	header('Access-Control-Allow-Methods:POST,GET');
	header('Access-Control-Allow-Headers:x-requested-with,content-type');

    $host = "https://free.niutrans.com";
    $path = "/NiuTransServer/translation";
    $apikey = "eb65376772d4b6ee195a8a8e23a582a1";
	$dictNo = "eed8d9d5d4";
	$memoryNo="71c53902f8";
    $src_text=urldecode($_POST["src"]);
	$from=$_POST["from"];
	$to=$_POST["to"];
    $durl = $host.$path."?from=". $from . "&src_text=".urlencode($src_text)."&to=" . $to . "&apikey=".$apikey."&dictNo=".$dictNo."&memoryNo=".$memoryNo;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $durl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $res = curl_exec($ch);
	
    curl_close($ch);
	
	//var_dump($res);
		
	header("Content-type: application/json");
	echo json_encode($res,JSON_UNESCAPED_UNICODE);
	die();

}


// Checks if the user exists in the database
function validateUser($userid, $userpassword) {
  // doing a user exists check with minimal to no validation on user input
  
	// if (!isset($userid) || !isset($userpassword) || empty($userid) || empty($userpassword)){
	// 	$res["uid"]=$userid;
	// 	$res["error"]=true;
	// 	$res["message"]="Authorized failed, invalid user id or password";
	// 	exit(json_encode($res,JSON_UNESCAPED_UNICODE));		
	// }
	
	// Create connection
	$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
	$conn->set_charset("utf8");
	
	// Check connection
	if ($conn->connect_error) {
		die("Database connection established Failed..");
	} 

	//update user log file

	$macaddress='';

	$result = $conn->query("INSERT INTO " . JWT_DBLOG . 
	"(userid, accessip, accessdtc, accessmac) 
	VALUES ('$userid', '" . $_SERVER["REMOTE_ADDR"] . "', '" . RUN_DTC . "', '$macaddress') ");

	//query the login user credentials
	// $userpasscode = md5(strtoupper($userid) . $userpassword);
	$where_condition=" upper(userid)='" . strtoupper($userid) . "' and (password='" . $userpassword . "' or passcode='" . $userpassword . "')" ;
	$result = $conn->query("SELECT userid, username, password, email, passcode, isadmin FROM " . JWT_DBUSER . " utf8 where " . $where_condition . " ");
	$no=$result->num_rows;

	
	if ($no==0){
		$res["jwt"]="";
		$res["uid"]=$userid;
		$res["error"]=true;		
		$res["message"]="Authorized failed, invalid user id or password";
		
		//close connection and output json object;
		$conn -> close();
		exit(json_encode($res));			
	}
	elseif($no>=2){
		$res["jwt"]="";
		$res["uid"]=$userid;
		$res["error"]=true;
		$res["message"]="Authorized failed, multiple users exist, system error.";

		//close connection and output json object;
		$conn -> close();
		exit(json_encode($res));			
	}
	
	//print($no);
	//print_r($result);
	$user = array();
	while ($row = $result->fetch_assoc()){
		array_push($user, $row);
	}
	
	//print_r($user);
  
  if ($userid == $user[0]['userid'] && ($userpassword == $user[0]['password'] or $userpassword == $user[0]['passcode'])) {
    // Add user email and id to empty email and id variable and return true 
		
	//$jwt = genJWT($userid);
	$res["uid"]=$userid;
	$res["error"]=false;
	$res["alevel"]=$user[0]['isadmin'] ;//role level, admin or not
	$res["email"]=$user[0]['email'] ;
	$res["username"]=$user[0]['username'] ;
	//$res['token']=decode($jwt, JWT_KEY); 
	//$jwt2 = encode(decode($jwt, JWT_KEY),JWT_KEY);
	$res["message"]="Authorized successfully";
	$res["token"]=genJWT($userid);
	//$res["jwt2"]=$jwt2;

	//close connection and output json object;
	$conn -> close();
    exit(json_encode($res));	
	
  } else {   
	$res["jwt"]="";
	$res["uid"]=$userid;
	$res["error"]=true;
	$res["alevel"]=0;
	$res["message"]="Auth to be redirected";
	exit(json_encode($res));	
	
  }
}

$iflogin=validateUser($userid, $userpassword);

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>