<?php

include "_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where xlrmfl <> 'Y' ";

//Reset the action when applicable;
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}


//for update operation;
if ($action == 'approve') {
	$id = $_POST['id'];

	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'APPROVED', `xlmodtc` = '$run_dt'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Approved successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Approve failed";
	}
}

if ($action == 'lock') {
	$id = $_POST['id'];

	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'LOCKED', `xlmodtc` = '$run_dt'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Locked successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Locked failed";
	}
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>