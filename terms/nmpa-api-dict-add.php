<?php

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'filtereditems';
$xltype = "";
$testcode = "";
$status = "";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}


if (isset($_POST['keylist']) && !empty($_POST['keylist']) ) {
	$keylist=urldecode($_POST['keylist']);
    $where_condition= " a.id in (" . $keylist .  ")";
}
else{
    $keylist = "";
    $where_condition= "1 > 0";
}

$res['keylist']=urldecode($_POST['keylist']);

$srcText=urldecode($_POST['xltest']);
$tgtText=urldecode($_POST['xlmodify']);

$res['keylist']=urldecode($_POST['keylist']);
                
$jobid =random_character($length = 8);
$run_dt=RUN_DTC;


$tbname = "_xd_nmpa_api_memory";

//call translation API;

header('content-type:application:json;charset=utf8');
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:POST,GET');
header('Access-Control-Allow-Headers:x-requested-with,content-type');

$host = "https://apis.niutrans.com";
$path = "/NiuTransServerDict/addDictionary";
$apikey = "eb65376772d4b6ee195a8a8e23a582a1";
$dictNo = "eed8d9d5d4";
$memoryNo="71c53902f8";

// $src_text=urldecode($query_list);
$from="en";
$to="zh";

$param=array(
    "fromText"=>"en",
    "toText"=>"zh", 
    "apikey"=>$apikey, 
    "dictNo"=>$dictNo, 
    "srcText"=>$srcText, 
    "tgtText"=>$tgtText
);

// fromText	    String	源语言：待翻译文本语种参数 支持语言列表
// toText	    String	目标语言：翻译目标语种参数 支持语言列表
// apikey	    String	API密钥 可在控制台查看
// srcText	    String	源语语言词汇
// tgtText	    String	目标语语言词汇
// dictNo       String	设置术语词典子库ID，缺省值为空

$param_string = http_build_query($param);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://apis.niutrans.com/NiuTransServerDict/addDictionary");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//     'Content-Type: application/json')
// );

curl_setopt($ch, CURLOPT_TIMEOUT, 30);

$translation_results = curl_exec($ch);

curl_close($ch);

$res['message']= $translation_results;
$res['sql']= $param_string;

//insert into temporary table to store the translation;
$protocolid = trim($_POST['protocolid']);
$studyid = trim($_POST['studyid']);
$xlcat = urldecode(trim($_POST['xlcat']));
$xlscat = urldecode(trim($_POST['xlscat']));
$xltest = urldecode(trim($_POST['xltest']));
$xltestcd = trim($_POST['xltestcd']);
$xlmodify = urldecode(trim($_POST['xlmodify']));
$xltype = urldecode(trim($_POST['xltype']));
$xlstat = "MEMORY";
$xlrmfl = "N";
$xlcrdtc = RUN_DTC;
$userid = $_POST['userid'];
$xlauditlog=RUN_DTC . " " . $_POST['userid'] ." added";

$tbname_api='_xd_nmpa_api_memory';

$sql = "INSERT INTO " . $tbname_api . 
"(`protocolid`, `studyid`, `xlcat`, `xlscat`, `xltype`, `xltest`, `xlmodify`,`xlcruser`, `xltestcd`, `xlstat`, `xlcrdtc`, `xlauditlog`) 
VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltype', '$xltest', '$xlmodify','$userid', '$xltestcd', '$xlstat', '$xlcrdtc', '$xlauditlog') ";

//execute the sql statement;
$result = $conn->query($sql);

if ($result) {
    $res['error'] = false;
    $res['message'] = "添加记忆库成功！";
    
}else{
    $res['error'] = true;
    $res['source']= $source;
    $res['target']= $target;
    // $res['sql']= $sql;
    $res['message'] = "添加记忆库失败！";
}


//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>