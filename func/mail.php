<?php

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include "../config/_init_.php";

cors();
chkJWT();

//Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
// $mail->Charset='UTF-8';

try {

	//retrieve the user email from database;


    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtpdm.aliyun.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'nmpa-toolkits@data42.cn';                     //SMTP username
    $mail->Password   = MAIL_PASSWORD;                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 465;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('nmpa-toolkits@data42.cn', 'Translation Service');
    $mail->addAddress($_POST['mailfrom'], $_POST['username']);     //Add a recipient
    // $mail->addAddress('medpower@gmail.com');               //Name is optional
    $mail->addReplyTo($_POST['mailfrom'], $_POST['username']);
    // $mail->addCC($_POST['mailfrom'], $_POST['username']);
    $mail->addBCC('zhijun.wei@novartis.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
	$body = 'Dear, <br><br>';
	$body = $body . 'Please help to review the following terms: <br><br>';
	$body = $body .'<table border="1" width="500" cellpadding="10" cellspacing="0" align="center">';
	$body = $body . urldecode($_POST['mailbody']);
	$body = $body . '</table>';
	$body = $body .'<br>'; 
	$body = $body .'<p style="margin-top: 1em; margin-bottom: 0; margin-left: 0; margin-right: 0;">' . $_POST['username'] . '</p>';
	$body = $body .'<p>Email: ' .   $_POST['mailfrom'] . '</p>';
    $body = $body .'Data42 Translation Service </p>';
	$body = $body .'<a href="https://data42.cn">https://data42.cn</a>';

    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = $_POST['mailsubject'];
    $mail->Body    = $body;
    $mail->AltBody = urldecode($_POST['mailbody']);

    $mail->send();
    // echo 'Message has been sent';
	$res['error'] = false;
	$res['message'] = "邮件发送成功！";
} catch (Exception $e) {
    // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	$res['error'] = true;
	$res['message'] = "邮件发送失败，请稍后再试！";
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>