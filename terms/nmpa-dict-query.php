<?php

require_once "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where xlrmfl <> 'Y' ";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$key =explode("-",$_POST['keyname']);

$keyvalue = urldecode($_POST['keyvalue']);
$key_where_condition = "0>1";
foreach ($key as $keyname){	
	$key_where_condition = $key_where_condition . " or " . $keyname . " like '" . $keyvalue . "'";
};


if (isset($_POST['keyvalue']) && !empty($_POST['keyvalue'])) {	
	$where_condition= $where_condition . " and (" . $key_where_condition . ")";
}


//For category fetching purpose;

//For read operation;
if ($action == 'read') {

	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " ");
	$records = array();
	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}
	$res['records'] = $records;
	$res['message'] = "Query successfully";
	$res['debug'] = "SELECT * FROM " . $tbname . " utf8 " . $where_condition . " ";
}



//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>