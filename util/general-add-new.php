<?php

include "../config/ndp/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);
$userid = $_POST['userid'];

//Reset the action when applicable;

if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$debug = 'N';

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}

if (isset($_POST['keytablename']) && !empty($_POST['keytablename'])) {
	$tbname=str_replace("-", "_", urldecode($_POST["keytablename"]));
}

$run_dt = RUN_DTC;

$column_type_sql="SELECT column_name,data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . JWT_DBNAME . "' AND TABLE_NAME = '$tbname';";
$column_type_result = $conn->query($column_type_sql);

$columns = array();
$types = array();

while ($row = $column_type_result->fetch_assoc()){
	array_push($columns, $row["column_name"]);
	array_push($types, $row["data_type"]);
}	

$updated_varlist="";

if (isset($_POST['varlist']) && !empty($_POST['varlist'])) {
	$updated_varlist=explode(' ', urldecode($_POST['varlist']));
}

//For creation operation;
if ($action == 'create') {

    $crdtc = RUN_DTC;
    $modtc = RUN_DTC;
    $auditlog = RUN_DTC . " " . $userid . " added";

    $sql_varlist ="";
    $sql_vallist ="";
    $seq=0;

	foreach ( $updated_varlist as $var) { 

		//determine the field type;
		$type= $types[array_search($var, $columns)];

		if ($type=='char' || $type=='text' || $type =='varchar'){
			$vartype='C';
		}
		else{
			$vartype='N';
		}

        $value=urldecode($_POST[$var]);

        if ($seq==0){
            $sql_varlist = $var;
            if($vartype=='C'){
                $sql_vallist = "'" . mysqli_real_escape_string($conn,$value) . "'" ;
            }
            else{
                $sql_vallist = mysqli_real_escape_string($conn,$value);
            }
        }
        else{
            $sql_varlist = $sql_varlist . ', ' . $var;         
              
            if($vartype=='C'){
                $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$value) . "'";     
            }
            else{
                $sql_vallist = $sql_vallist . ', ' .  mysqli_real_escape_string($conn,$value) ;     
            }
        };                
        $seq+=1;

	} 
	
    //derive the sql variable list;
    $sql_varlist = "INSERT INTO " . $tbname . 
    "(uuid, " . $sql_varlist . ", cruserid, mouserid, crdtc, modtc, auditlog) \n";

    $sql = $sql_varlist . 'values (uuid(), ' .  $sql_vallist . ", '$userid', '$userid', '$crdtc', '$modtc', '$auditlog' ); \n ";

    if ($debug=='Y'){
        $res['sql']=$sql;
    }

    $result = $conn->query($sql);
            
        if ($result) {
            $res['message'] = "New record added successfully!";
        } else{
            $res['error'] = true;
            $res['message'] = "New record added failed!";
        }


}
else{
	$res['error'] = true;
	$res['message'] = "无效操作，请确认！";
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>