<?php

	include "../config/_init_.php";
	include "../func/translate.php";
	
	cors();
	
	chkJWT();

    $query=urldecode($_POST["src"]);	
	
	$from=($_POST["from"]);
	$to=($_POST["to"]);
	
    $res = translate($query, $from, $to);
	
	//print_r($res);
		
	//var_dump($res);
		
	header("Content-type: application/json");
	echo json_encode($res,JSON_UNESCAPED_UNICODE);
	die();
?>