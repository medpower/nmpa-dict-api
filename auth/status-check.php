<?php

require_once "../config/_init_.php";

use \Firebase\JWT\JWT;

cors();

$res = array('error' => false);

$res['token']=$_SERVER['HTTP_AUTHORIZATION'];


// Generates and signs a JWT for User
function genJWT($userid) {
  // Make an array for the JWT Payload
	global $auth_key;
	$payload = array(
		"iss" => $_SERVER['HTTP_HOST'],
		"aud" => $_SERVER['HTTP_HOST'],
		"iat" => $_SERVER['REQUEST_TIME'], //issue time
		"exp" => $_SERVER['REQUEST_TIME'] + (60 * 60), //expire time
		"nbf" => 1357000000, //not used before
		"user" => $userid,
		"jti" => md5(uniqid('DATA42-JWT-').$_SERVER['REQUEST_TIME']) //unique token identifier
	);
 
	// encode the payload using our secretkey and return the token
	return JWT::encode($payload, $auth_key);
}

// Checks if the user exists in the database
function validateUser($userid, $userpassword) {
  // doing a user exists check with minimal to no validation on user input
  
	if (!isset($userid) || !isset($userpassword) || empty($userid) || empty($userpassword)){
		$res["uid"]=$userid;
		$res["error"]=true;
		$res["msg"]="Authorized failed, invalid user id or password";
		exit(json_encode($res));		
	}
	
	global $servername,$username,$password,$dbname,$tbname_users;
  
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	$conn->set_charset("utf8");
	
	// Check connection
	if ($conn->connect_error) {
		die("Database connection established Failed..");
	} 

	$where_condition=" userid='" . $userid . "' and password='" . $userpassword . "'" ;
	$result = $conn->query("SELECT userid, password, email, passcode FROM " . $tbname_users . " utf8 where " . $where_condition . " ");
	$no=$result->num_rows;
	//close connection and output json object;
	$conn -> close();
	
	if ($no==0){
		$res["jwt"]="";
		$res["uid"]=$userid;
		$res["error"]=true;
		$res["msg"]="Authorized failed, invalid user id or password";
		exit(json_encode($res));			
	}
	elseif($no>=2){
		$res["jwt"]="";
		$res["uid"]=$userid;
		$res["error"]=true;
		$res["msg"]="Authorized failed, multiple users exist, system error.";
		exit(json_encode($res));			
	}
	
	//print($no);
	//print_r($result);
	$user = array();
	while ($row = $result->fetch_assoc()){
		array_push($user, $row);
	}
	
	//print_r($user);
  
  if ($userid == $user[0]['userid'] && $userpassword == $user[0]['password']) {
    // Add user email and id to empty email and id variable and return true 
		
	$jwt = genJWT($userid);
	$res["jwt"]=$jwt;
	$res["uid"]=$userid;
	$res["error"]=false;
	$res["msg"]="Authorized successfully";
    exit(json_encode($res));	
	
  } else {   
	$res["jwt"]="";
	$res["uid"]=$userid;
	$res["error"]=true;
	$res["msg"]="Authorized failed";
	exit(json_encode($res));	
	
  }
}

//$iflogin=validateUser($userid, $userpassword);

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>