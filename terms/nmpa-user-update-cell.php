<?php

include "../config/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$action = $_POST['action'];
$username_cn = $_POST['username_cn'];
$xlauditlog=$_POST['xlauditlog'];
$userid=strtoupper($_POST['userid']);

$tbname = "_xd_nmpa_user_list";

//for cell update operation;
if ($action == 'cell-update') {
	$uid = $_POST['uid'];	
	$xlmodtc = RUN_DTC;
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated';
	
	$sql ="UPDATE " . $tbname . " 
	SET `username_cn` = '$username_cn',
		`xlmouser` = '$userid',
		`xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog'
	WHERE `uid` = '$uid'
	";

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "Cell updated successfully";
		// $res['auditlog']=$xlauditlog;
		

	} else{
		$res['error'] = true;
		$res['message'] = "Cell updated failed";
		$res['debug'] = "Not applicable";
		// $res['auditlog']=$xlauditlog;
		$res['sql']=$sql;
	}
}


//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>