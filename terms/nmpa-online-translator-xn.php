<?php

    include_once "../config/_init_.php";
    //use \Firebase\JWT\JWT;

    cors();

    chkJWT();

	header('content-type:application:json;charset=utf8');
	header('Access-Control-Allow-Origin:*');
	header('Access-Control-Allow-Methods:POST,GET');
	header('Access-Control-Allow-Headers:x-requested-with,content-type');

    $host = "https://free.niutrans.com";
    $path = "/NiuTransServer/translation";
    $apikey = "eb65376772d4b6ee195a8a8e23a582a1";
	$dictNo = "eed8d9d5d4";
	$memoryNo="71c53902f8";
    $src_text=urldecode($_POST["src"]);
	$from=$_POST["from"];
	$to=$_POST["to"];
    $durl = $host.$path."?from=". $from . "&src_text=".urlencode($src_text)."&to=" . $to . "&apikey=".$apikey."&dictNo=".$dictNo."&memoryNo=".$memoryNo;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $durl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $res = curl_exec($ch);
	
    curl_close($ch);
	
	//var_dump($res);
		
	header("Content-type: application/json");
	echo json_encode($res,JSON_UNESCAPED_UNICODE);
	die();
?>