<?php

include "../config/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;

$action = $_POST['action'];
$xlauditlog=$_POST['xlauditlog'];
$where_condition="where xlrmfl <> 'Y' ";

//for cell update operation;
if ($action == 'row-update') {
	$id = $_POST['id'];
	$xlcat = $_POST['xlcat'];
	$xlscat = $_POST['xlscat'];
	$xltest = $_POST['xltest'];
	$xltestcd = $_POST['xltestcd'];
	$xlmodify = $_POST['xlmodify'];
	$xlstat = $_POST['xlstat'];
	$xlrmfl = $_POST['xlrmfl'];
	$xlmodtc = RUN_DTC;
	$xlcomment = $_POST['xlcomment'];
    $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated from ' . $_POST['original-term'] . ", " . $_POST['original-status'] . ", " . $_POST['original-comment'] .'===>' . 
                                $xlmodify . ", " . $xlstat . ", " . $xlcomment;
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlcat` = '$xlcat', `xlscat` = '$xlscat', `xltest` = '$xltest', `xleval` = 'MANUAL',
						`xltestcd` = '$xltestcd', `xlstat` = '$xlstat', `xlmodify` = '$xlmodify',
						`xlrmfl` = '$xlrmfl', `xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog',
						`xlcomment` = '$xlcomment'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Row updated successfully";
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "Updated failed";
		$res['debug'] = "Not applicable";
        $res['auditlog']=$xlauditlog;
	}
}

$res['debug'] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>