<?php

include "../config/_init_.php";

cors();
chkJWT();


// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);


//Initialize the action as read;
$action = 'upload-csv';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}


if (isset($_FILES['file']['tmp_name']) && !empty($_FILES['file']['tmp_name'])) {
	$file = $_FILES['file'];
	$res["filename"]=$_FILES['file']['name'];	

	$targetSubFolder=strtolower($userid);
	$strFileUploadDateTime=date("Y-m-dTH-i-s",filemtime($file["tmp_name"]));
	$strFileUploadDate=date("Y-m",filemtime($file["tmp_name"]));

    $destination="../upload";
	
	$destination=$destination.DIRECTORY_SEPARATOR.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR;

    if (!is_dir($destination)) {
        mkdir("$destination",0777,true);
        chmod("$destination", 0777);
    }

    $fileName = iconv('utf-8', 'gb2312', $file['name']);
    move_uploaded_file($file['tmp_name'], $destination.$fileName);

    $res["message"]="术语上传成功！";	
    $res["pathname"]=$destination.$fileName;	

	}	
else{
	//echo "请选择上传文件";
}


//单个文件移动
function moveFile($file,$destination='../upload/'){

	$targetSubFolder=strtolower($userid);
	$strFileUploadDateTime=date("Y-m-dTH-i-s",filemtime($file["tmp_name"]));
	$strFileUploadDate=date("Y-m",filemtime($file["tmp_name"]));
	
	$destination=$destination.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR;

	if (checkPost($file['tmp_name']) && checkError($file['error']) && checkExt($file['name']) && checkSize($file['size'])) {
		//所有验证通过，移动文件到指定目录
		//检验指定目录是否存在，如果不存在则创建目录
		if (!is_dir($destination)) {
			mkdir("$destination",0777,true);
			chmod("$destination", 0777);
		}
		$fileName = iconv('utf-8', 'gb2312', $file['name']);
		//$fileName =$file['name'];
		if (move_uploaded_file($file['tmp_name'], $destination.$fileName)) { 		
            $res["filepath"]=$destination.$fileName;
			return true;			
		}else{
			return false;
		}
							  
	}else{
		return false;
	} 
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>