<?php

include "./_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Reset the action when applicable;

if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$tbname = '_xd_nmpa_study_info';
$tbname2 = '_xd_nmpa_study_task';

//For creation operation;
if ($action == 'create') {
	$protocolid = strtoupper(trim($_POST['protocolid']));
	$studyid = strtoupper(trim($_POST['studyid']));
	$studytitle = trim($_POST['studytitle']);
    $indication = trim($_POST['indication']);
    $taname = trim($_POST['taname']);

    $xledctype = trim($_POST['xledctype']);
    $xldttype = trim($_POST['xldttype']);
    $xlspectype = trim($_POST['xlspectype']);
    $xlcrftype = trim($_POST['xlcrftype']);

	$xldopoc = trim($_POST['xldopoc']);
	$xlrapoc = trim($_POST['xlrapoc']);
	$xlsppoc = trim($_POST['xlsppoc']);
	$xldmpoc = trim($_POST['xldmpoc']);
    $xcdpoc = trim($_POST['xlcdpoc']);
    $xlmwpoc = trim($_POST['xlmwpoc']);

    // $xlendtc =  date('Y-m-d',date_create($_POST['xlendtc']));
    // $xldpdtcp = date('Y-m-d',date_create($_POST['xldpdtcp']));
    // $xltcdtcp = date('Y-m-d',date_create($_POST['xltcdtcp']));
    // $xldadtc =  date('Y-m-d',date_create($_POST['xldadtc']));

    $xlendtc = $_POST['xlendtc1'];
    $xldpdtcp =$_POST['xldpdtcp1'];
    $xltcdtcp =$_POST['xltcdtcp1'];
    $xldadtc = $_POST['xldadtc1'];

	$xlstat = "INITIAL";
	$xlfl = "N";
    $xlcruserid = strtoupper(trim($_POST['userid']));
    $xlmouserid = strtoupper(trim($_POST['userid']));
	$xlcrdtc = RUN_DTC;
	$xlmodtc = RUN_DTC;
	$xlcomment = trim($_POST['xlcomment']);
	
	//determine whether it already exists or not;
	$sql="select id from " . $tbname . 
	" where protocolid = '" . $protocolid . "' and studyid ='" . $studyid . "'";
	$result = $conn->query($sql);
	$num    = $result -> num_rows;  
	if ($num > 0 ){
		$res['error'] = false;
		// $res['sql'] = $sql;
		$res['count'] = $num;
		$res['message'] = "当前项目已注册，请确认！";		
	}
	else{
        //insert study level information
        $sql = "INSERT INTO " . $tbname . 
        "(`protocolid`, `studyid`,`studytitle`, `indication`, `taname`, 
        `xledctype`, `xldttype`, `xlspectype`,`xlcrftype`,
        `xldopoc`, `xlrapoc`, `xlsppoc`,`xldmpoc`,`xlcdpoc`, `xlmwpoc`, 
        `xlcruserid`, `xlmouserid`, 
        `xlendtc`,`xldadtc`, `xldpdtcp`, `xltcdtcp`, 
        `xlstat`, `xlfl`,`xlcrdtc`,	`xlmodtc`, `xlcomment`) 
        VALUES ('$protocolid', '$studyid','$studytitle', '$indication', '$taname', 
        '$xledctype', '$xldttype','$xlspectype', '$xlcrftype', 
        '$xldopoc', '$xlrapoc','$xlsppoc', '$xldmpoc', '$xlcdpoc', '$xlmwpoc', 
        '$xlcruserid', '$xlmouserid',
        '$xlendtc', '$xldadtc', '$xldpdtcp','$xltcdtcp',
        '$xlstat', '$xlfl', '$xlcrdtc','$xlmodtc', '$xlcomment') ";

		$result = $conn->query($sql);
			
		if ($result) {
			$res['message'] = "项目注册成功";
		} else{
			$res['error'] = true;
            // $res['sql'] = $sql;
			$res['message'] = "项目注册失败";
		}


        if($xldttype==='CDISC'){

            //insert the details into tracker
            $sql = "INSERT INTO " . $tbname2 . 
            "(`protocolid`, `studyid`, 
            `xltype`, `xlcat`, `xlscat`, `xlstat`,
            `xlstdtcp`, `xlendtcp`, 
            `xlcruser`, `xlmouser`, `xleditor`, `xlreviewer`, `xlapprover`, 
            `xlnafl`,`xlcrdtc`,	`xlmodtc`, `xlcomment`) VALUES " .
            " 
            ('$protocolid', '$studyid',
            'Dataset', 'Label','SDTM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Dataset', 'Label','ADaM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                     
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Label','SDTM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                         
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Variable', 'Label','ADaM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
                      
            ('$protocolid', '$studyid',
            'aCRF', 'Question','General', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xldmpoc', '$xlcdpoc','$xldmpoc',                    
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Derivation','SDTM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                         
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Variable', 'Derivation','ADaM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Codelist','ADaM', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Document', 'Reviewer Guide','SDRG', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                         
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Document', 'Reviewer Guide','ADRG', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Terminology','Drug Name', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment')

            ";
        }
        else{
            //insert the details into tracker
            $sql = "INSERT INTO " . $tbname2 . 
            "(`protocolid`, `studyid`, 
            `xltype`, `xlcat`, `xlscat`, `xlstat`,
            `xlstdtcp`, `xlendtcp`, 
            `xlcruser`, `xlmouser`, `xleditor`, `xlreviewer`, `xlapprover`, 
            `xlnafl`,`xlcrdtc`,	`xlmodtc`, `xlcomment`) VALUES " .
            " 
            ('$protocolid', '$studyid',
            'Dataset', 'Label','Raw Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Dataset', 'Label','Analysis Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                     
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Label','Raw Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                         
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Variable', 'Label','Analysis Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
                    
            ('$protocolid', '$studyid',
            'aCRF', 'Question','General', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xldmpoc', '$xlcdpoc','$xldmpoc',                    
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Derivation','Raw Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                         
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 
            
            ('$protocolid', '$studyid',
            'Variable', 'Derivation','Analysis Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Codelist','Analysis Data', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment'), 

            ('$protocolid', '$studyid',
            'Variable', 'Terminology','Drug Name', 'INITIAL', 
            '$xldadtcp', '$xltcdtcp',
            '$xlcruserid', '$xlmouserid', '$xlsppoc', '$xldopoc','$xlsppoc',                 
            'Y', '$xlcrdtc','$xlmodtc', '$xlcomment')

            ";
        }

		$result = $conn->multi_query($sql);
			
		if ($result) {
			$res['message'] = "项目任务注册成功";
		} else{
			$res['error'] = true;
            $res['sql'] = $sql;
			$res['message'] = "项目任务注册失败";
		}

}
}


//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>