<?php

$servername = "hdm710887449.my3w.com";
$username   = "hdm710887449";
$password   = "Wsy$031209";
$dbname     = "hdm710887449_db";

$tbname ="_xd_nmpa_dictionary_global_terms";

$run_dt = date("Y-m-d") . "T" . date("H-i-s");

date_default_timezone_set('PRC');

//判断文件大小,up to 20MB, but also limted by the settings in APACHE; 
function checkSize($size, $maxSize=20971520){
	if ($size>$maxSize) {
		$res["message"] = '上传文件过大，请确认后再重新上传！';
		return false;
	}else{
		return true;
	}
}


//判断文件是否为运行上传类型
function checkExt($fileName, $ext = ['xls', 'xlsx', 'csv', 'txt']){ 
	if (!in_array(strtolower(pathinfo($fileName)['extension']), $ext)) {
		$res["message"] = '不支持该类型文件上传，请确认后再重新上传！';
		return false;
	}else{
		return true;
	} 
}

//上传文件错误号判断
function checkErroe($error){
	if ($error>0) {
		switch ($error) {
			case 1:
				$ms = '上传失败，文件超过文件最大上传限制！';
				break;
			case 2:
				$ms = '上传失败，文件超过文件最大上传限制！';
				break;
			case 3:
				$ms = '部分文件上传失败，请稍后重试！';
				break; 
			case 4:
				$ms = '上传失败，没有文件被上传！';
				break; 
			case 6:
				$ms = '上传失败，找不到临时文件夹！';
				break;
			case 7:
				$ms = '上传失败，写入失败！';
				break;
			case 8:
				$ms = '上传失败，系统错误！';
				break;     
		}
		$res["message"] = $ms;
		return false;
	}else{
		return true;
	}         
}

//检验是否通过 POST 上传
function checkPost($tmpName){
	if (is_uploaded_file($tmpName)) {
		return true;
	}else{
		$res["message"] = '非法操作，请确认后重试！';
		return false;
	}
}

//上传文件信息整理
function getFilesInfo($filesInfo){
	if (is_array($filesInfo['name'])) {
		$fileArr = [];
		$i=0;
		$filesNum = count($filesInfo['name']);
		for($i=0;$i<$filesNum;$i++){
			$fileArr[$i]=[
				'name' => $filesInfo['name'][$i],
				'type' => $filesInfo['type'][$i],
				'tmp_name' => $filesInfo['tmp_name'][$i],
				'error' => $filesInfo['error'][$i],
				'size' => $filesInfo['size'][$i]
			];
		}
		return $fileArr;
	}else{
		return false;
	}
}

function object2array(&$object) {
    $object =  json_decode( json_encode( $object),true);
    return  $object;
}

function cors() {
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept");

        exit(0);
    }
}



?>