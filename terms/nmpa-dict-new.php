<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Reset the action when applicable;

if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$run_dt = RUN_DTC;

//For creation operation;
if ($action == 'create') {
	$protocolid = trim($_POST['protocolid']);
	$studyid = trim($_POST['studyid']);
	$xlcat = trim($_POST['xlcat']);
	$xlscat = trim($_POST['xlscat']);
	$xltest = trim($_POST['xltest']);
	$xltestcd = trim($_POST['xltestcd']);
	$xlmodify = trim($_POST['xlmodify']);
	$xlstat = "ACTIVE";
	$xlrmfl = "N";
	$xlcrdtc = $run_dt;
	$xlmodtc = $run_dt;
	$xlcomment = trim($_POST['xlcomment']);
	
	//determine whether it already exists or not;
	$sql="select id from " . $tbname . 
	" where xltest = '" . $xltest . "' and xlrmfl <> 'Y'";
	$result = $conn->query($sql);
	$num    = $result -> num_rows;  
	if ($num > 0 ){
		$res['error'] = false;
		$res['sql'] = $sql;
		$res['count'] = $num;
		$res['message'] = "Item already exists, operation ignored";		
	}
	else{
		$result = $conn->query("INSERT INTO " . $tbname . 
					"(`protocolid`, `studyid`,`xlcat`, `xlscat`, `xltest`, `xlmodify`,
					`xltestcd`, `xlstat`, `xlrmfl`,`xlcrdtc`,
					`xlmodtc`, `xlcomment`) 
			VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltest', '$xlmodify',
					'$xltestcd', '$xlstat', '$xlrmfl','$xlcrdtc',
					'$xlmodtc', '$xlcomment') ");
			
		if ($result) {
			$res['message'] = "术语添加成功！";
		} else{
			$res['error'] = true;
			$res['message'] = "术语添加失败！";
		}
}}
elseif ($action == 'rowcopy') {
	$protocolid = trim($_POST['protocolid']);
	$studyid = trim($_POST['studyid']);
	$xlcat = trim($_POST['xlcat']);
	$xlscat = trim($_POST['xlscat']);
	$xltest = trim($_POST['xltest']);
	$xltestcd = trim($_POST['xltestcd']);
	$xlmodify = trim($_POST['xlmodify']) . "(Copy)";
	$xlstat = "Active";
	$xlrmfl = "N";
	$xlcrdtc = $run_dt;
	$xlmodtc = $run_dt;
	$xlcomment = trim($_POST['xlcomment']);
	$xlauditlog=$run_dt . " created with a copy of existing item " . trim($_POST['xltest']);
	
	$result = $conn->query("INSERT INTO " . $tbname . 
				"(`protocolid`, `studyid`,`xlcat`, `xlscat`, `xltest`, `xlmodify`,
				`xltestcd`, `xlstat`, `xlrmfl`,`xlcrdtc`,
				`xlmodtc`, `xlcomment`, `xlauditlog`) 
		VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltest', '$xlmodify',
				'$xltestcd', '$xlstat', '$xlrmfl','$xlcrdtc',
				'$xlmodtc', '$xlcomment', '$xlauditlog') ");
		
	if ($result) {
		$res['message'] = "术语添加成功！";
	} else{
		$res['error'] = true;
		$res['message'] = "术语添加失败！";
	}
}
elseif ($action == 'append') {
	$tbname = "_xd_nmpa_global_" . explode('-',$_POST['keytablename'])[1] . '_' . explode('-',$_POST['keytablename'])[2];
	$protocolid = urldecode($_POST['protocolid']);
	$studyid = urldecode($_POST['studyid']);
	$xlcat = urldecode($_POST['xlcat']);
	$xlscat = urldecode($_POST['xlscat']);
	$xltest = urldecode($_POST['xltest']);
	$xltestcd = urldecode($_POST['xltestcd']);
	$xlmodify = urldecode($_POST['xlmodify']) ;
	$xlstat = "PENDING";
	$xlrmfl = "N";
	$xlcrdtc = RUN_DTC;
	$xlmodtc = '';
	$xlcomment = urldecode($_POST['xlcomment']);
	$userid = $_POST['userid'];
	$xlauditlog=$run_dt . " " . $_POST['userid'] ." appended";

	//determine whether it already exists or not;
	$sql="select id from " . $tbname . 
	" where xltest = '" . $xltest . "' and xlrmfl <> 'Y'";
	$result = $conn->query($sql);
	$num    = $result -> num_rows;  
	if ($num > 0 ){
		$res['error'] = true;
		$res['sql'] = $sql;
		$res['count'] = $num;
		$res['message'] = "术语已存在，入库操作已取消！";		
	}
	else{	
		$sql = "INSERT INTO " . $tbname . 
		"(`protocolid`, `studyid`,`xlcat`, `xlscat`, `xltest`, `xlmodify`,`xlcruser`,
		`xltestcd`, `xlstat`, `xlrmfl`,`xlcrdtc`,
		`xlmodtc`, `xlcomment`, `xlauditlog`) 
	VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltest', '$xlmodify','$userid',
		'$xltestcd', '$xlstat', '$xlrmfl','$xlcrdtc',
		'$xlmodtc', '$xlcomment', '$xlauditlog') ";

		$result = $conn->query($sql);


		$res['sql'] = $sql;

		if ($result) {
			$res['error'] = false;
			$res['message'] = "术语入库成功！";
		} else{
			$res['error'] = true;
			$res['message'] = "术语入库失败！";
		}
	}
}
else{
	$res['error'] = true;
	$res['message'] = "无效操作，请确认！";
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>