<?php

include_once "../config/_init_.php";

// use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


//Initialize the action as read;

$action = 'read';

$userid=strtolower($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

$keytablename=strtolower($_POST["keyvalue"]);
$tbname = '_xd_nmpa_' . $keytablename;

$where_condition = '1>0';


// $res['sql'] = "SELECT * FROM " . $tbname . " utf8 where " . $where_condition . " ";

if ($keytablename=='user_log_view'){
    $sql='select * from ' . $tbname . " utf8  where " . $where_condition . " order by accessdtc desc, userid ";
}
else if($keytablename=='user_list'){
    $sql='select * from ' . $tbname . " utf8  where " . $where_condition . " order by regdate desc, userid ";
}
else if($keytablename=='study_info'){
    $sql='select * from ' . $tbname . " utf8  where " . $where_condition . " order by xlmodtc desc, protocolid, studyid";
}
else if($keytablename=='study_task'){
    $sql='select * from ' . $tbname . " utf8  where " . $where_condition . "  order by xlmodtc desc, protocolid, studyid";
}

// $sql='select * from ' . $tbname . " utf8  where " . $where_condition . "  ";

//For read operation;
if ($action == 'read') {
	
	// $res['sql'] = $where_condition;

	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

    //close connection and output json object;
	$conn -> close();
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $where_condition;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>