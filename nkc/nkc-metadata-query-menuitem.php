

<?php

include_once "../config/nkc/_init_.php";

//use \Firebase\JWT\JWT;

cors();

// chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


//Initialize the action as read;

$action = 'read';
$userid=strtoupper($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

$tbname = "_xl_dict_global_metadata";

$key =explode("-",$_POST['keyname']);
$keyvalue = urldecode($_POST['keyvalue']);
$where_condition = "where 1>0 ";

$res['kn'] = $key[0];
$res['kv'] = $keyvalue;

$key_where_condition = "0>1";
foreach ($key as $keyname){	
	$key_where_condition = $key_where_condition . " or " . $keyname . " like '" . $keyvalue . "'";
};

$limitResults='Y';

if (isset($_POST['keyvalue']) && !empty($_POST['keyvalue'])) {	
    $where_condition= $where_condition . " and (" . $key_where_condition . ")";
    $limitResults='N';
}

// $res['sql'] = $where_condition;
//For read operation;
if ($action == 'read') {

    if ($limitResults==='Y'){
		$sql = "SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by dsname, name limit 1000";
	}
	else{
		$sql = "SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by dsname, name ";
	}
	
	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

	//close connection and output json object;
	$conn -> close();
	
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $sql;//decode($userToken, JWT_KEY);
    $res['message'] = "Query successfully";
    // $res['kn'] = $key[0];
    // $res['kv'] = $keyvalue;
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>