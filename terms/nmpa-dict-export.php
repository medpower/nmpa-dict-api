<?php

require_once "_init_.php";

	/*
	 * echo the input array as csv data maintaining consistency with most CSV implementations
	 * - uses double-quotes as enclosure when necessary
	 * - uses double double-quotes to escape double-quotes
	 * - uses CRLF as a line separator
	 */
	 
	function echocsv($fields)
	{
	  $separator = '';
	  foreach ($fields as $field) {
		if (preg_match('/\\r|\\n|,|"/', $field)) {
	 $field = '"' . str_replace('"', '""', $field) . '"';
		}
		echo $separator . $field;
		$separator = ',';
	  }
	  echo "\r\n";
	}
	
cors();

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$action = 'export';
$filetype = "csv";
$where_condition="where xlrmfl <> 'Y' ";

//Reset the action when applicable;
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}


if (isset($_GET['filetype']) && !empty($_GET['filetype'])) {	
	$testname=urldecode($_GET['testname']);
	$where_condition= $where_condition . " and (xltest like '" . $testname . "'
					or xltestcd like '" . $testname . "' or 
					xlmodify like '" . $testname . "')";
}


//For category fetching purpose;


//For read operation;
if ($action == 'export') {
	$result = $conn->query("SELECT distinct xltest as SOURCE, xlmodify as TARGET FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$records = array();

	//$res['records'] = $records;
	
	//Give our CSV file a name.
	$filename = date("Y-m-d") . "T" . date("H-i-s") . ".csv";
	
	$destination="./download/";
	
	if (!is_dir($destination)) {
		mkdir("$destination",0777,true);
		chmod("$destination", 0777);
	}
	
	$csvFileName = $destination. $filename;
	
	$res['filename'] = $csvFileName;
	
		
	header('Content-Type: text/csv; charset=cp1252');
	header('Content-Disposition: attachment;filename=' . $filename);
	
	$seq=1;
	while ($row = $result->fetch_assoc()){
		//array_push($records, $row);
		if ($seq==1){
			echocsv(array_keys($row));
		};
		echocsv($row);
		$seq+=1;
	}
	
	/* 
	 * output header row (if atleast one row exists) 
	 */
	

	
	 
	//Open file pointer.
	//$fp = fopen($csvFileName, 'w');
	
	//$string = "SOURCE,TARGET\n";
    //foreach($records as $key => $value) {
      //  $string .= $value['SOURCE'] . ',' . "\t"  . $value['TARGET'] . "\t\n";
    //}
	//
	//echo $string;
	
	//$string =iconv('utf-8', 'gb2312', $string);
	 
	 
	//Loop through the associative array.
	//foreach($records as $row){
		//Write the row to the CSV file.
		//$row=iconv("utf-8","gb2312",$row);


		//fputcsv($fp, $string);
}
	 
	//Finally, close the file pointer.
	//fclose($fp);
	



//close connection and output json object;
$conn -> close();

?>