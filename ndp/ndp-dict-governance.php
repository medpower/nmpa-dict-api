<?php

include "../config/ndp/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where rmfl <> 'Y' ";
$xlauditlog=$_POST['original-auditlog'];
$run_dt=RUN_DTC;

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = strtolower($_POST['action']);
}

$debug = 'N';

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}

$id = $_POST['id'];
$userid = $_POST['userid'];

//for update operation;
if ($action == 'approve') {

	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to APPROVED';
	$auditlog=RUN_DTC . " " . $_POST['userid'] . ' updated status to APPROVED';
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`status` = 'APPROVED', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Approved', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Approved successfully";
		$res['error'] = false;
		$res['debug'] = "Not applicable";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Approved failed";
		$res['debug'] = "Not applicable";		
	}
}

//for global pending terms lock purpose;
if ($action == 'lockg') {	
	
	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status to LOCKED';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`status` = 'LOCKED', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Locked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Locked failed";
	}
}

//for study level terms lock purpose;
if ($action == 'lock') {	
	
	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status to LOCKED';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`status` = 'LOCKED', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Locked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Locked failed";
	}
}

if ($action == 'unlock') {	
	
	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from LOCKED to ACTIVE';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status to ACTIVE';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`status` = 'ACTIVE', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Unlocked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Unlocked failed";
	}
}

if ($action == 'pending') {

	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to PENDING for inputs';
	$auditlog = RUN_DTC . " " . $_POST['userid'] . ' updated status to PENDING for inputs';
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`stat` = 'PENDING', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Pending for inputs', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Status updated successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Status updated failed";
	}
}

if ($action == 'on-hold') {
	
	// $xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to ON-HOLDE for discussion';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status to ON-HOLDE';
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `mouserid` = '$userid',`status` = 'ON-HOLD', `modtc` = '$run_dt'
					,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
					WHERE `ndp_id` = '$id' and `rmfl` <> 'Y'
					");

	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Need discussion', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Status updated successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Status updated failed";
	}
}

if ($action == 'delete') {
	
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' deleted logically';

	$sql="UPDATE " . $tbname . " 
		SET `mouserid` = '$userid', `modtc` = '$run_dt',
		`rmfl` = 'Y'
		,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$auditlog')
		WHERE `ndp_id` = '$id' and upper(cruserid) = '" . strtoupper($userid). "' 
		";

	if ($debug=='Y'){
		$res['sql']=$sql;
	}
	
	$result = $conn->query($sql);
    $num    = $result -> num_rows;  
	if ($result) {
		$res['message'] = "Deleted successfully";
		$res['auditlog']=$auditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Deleted failed, not authorized!";
	}
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>