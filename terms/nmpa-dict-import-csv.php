<?php

include "_init_.php";


cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
$res = array('error' => true);

//Initialize the action as read;
$action = 'upload';

//Reset the action when applicable;
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}

if (isset($_FILES['file']['tmp_name']) && !empty($_FILES['file']['tmp_name'])) {
	$file = $_FILES['file'];
	$res["filename"]=$_FILES['file']['name'];	
	if (moveFile($file)==true){
		$res["message"]="File upload successfully";
	}else{
		$res["message"]="File upload failed";
	};
	$res["status"]="done";	
	}	
else{
	//echo "请选择上传文件";
}


//单个文件移动
function moveFile($file,$destination='./upload/'){

	//$targetSubFolder=$_POST["sub-folder"];
	//echo $targetSubFolder;
	
	$targetSubFolder="";
	$strFileUploadDateTime=date("Y-m-dTH-i-s",filemtime($file["tmp_name"]));
	$strFileUploadDate=date("Y-m",filemtime($file["tmp_name"]));
	
	if ($targetSubFolder !=""){
		$destination=$destination.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR;
	}
	else {
		$destination=$destination.$strFileUploadDate.DIRECTORY_SEPARATOR;
	}
	

	if (checkPost($file['tmp_name']) && checkErroe($file['error']) && checkExt($file['name']) && checkSize($file['size'])) {
		//所有验证通过，移动文件到指定目录
		//检验指定目录是否存在，如果不存在则创建目录
		if (!is_dir($destination)) {
			mkdir("$destination",0777,true);
			chmod("$destination", 0777);
		}
		$fileName = iconv('utf-8', 'gb2312', $file['name']);
		//$fileName =$file['name'];
		if (move_uploaded_file($file['tmp_name'], $destination.$fileName)) { 

			// $file= 'e:/server/wamp/www/test/upload/2021-02/LEE_lbl_trans.csv';
			// $file= 'http://127.0.0.1/test/upload/2021-02/LEE_lbl_trans.csv';

			$handle = fopen($destination.$fileName, 'r');

			if (!$handle) {
				exit('读取文件失败');
				$res["desc"]="读取文件失败: $destination.$fileName";
			}
			
			$fileinfo=explode("_",explode(".", $fileName)[0]);

			$i=1;
			
			$run_dt = date("Y-m-d") . "T" . date("H-i-s");
			// Create connection
			
			$servername = "hdm710887449.my3w.com";
			$username   = "hdm710887449";
			$password   = "Wsy$031209";
			$dbname     = "hdm710887449_db";
			$tbname ="_xd_nmpa_dictionary_global_terms";

			$conn = new mysqli($servername, $username, $password, $dbname);

			while (($data = fgetcsv($handle)) !== false) {
				// 下面这行代码可以解决中文字符乱码问题
				//$data[0] = iconv('gbk', 'utf-8', $data[0]);

				if ($data){
				// 跳过第一行标题
				if ($data[0] == 'source' or $data[0] == 'SOURCE') {
					continue;
				}
				
				//$item_list[] = $data;

				// data 为每行的数据，这里转换为一维数组
				//print count($data);
				//print_r($data);// Array ( [0] => tom [1] => 12 )
				
				$protocolid=$fileinfo[0];
				$studyid=$fileinfo[1];
				$xlcat=$fileinfo[2];
				$xlscat = $fileinfo[3];;
					
				$xltestcd = $data[0];
				$xltest = $data[0];
				$xlmodify = $data[1];
				$xlstat = "Active";
				$xlrmfl = "N";
				$xlcrdtc = $run_dt;
				$xlmodtc = $run_dt;
				$xlcomment = "";
				$xlauditlog = $run_dt . " system imported";

				//only add unique value;
				$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
						  " where xltest = '" . $xltest . "' and xlmodify = '" . $xlmodify . "'");
						  
				$no=$result->num_rows;		

				if ($no==0){

					$result = $conn->query("INSERT INTO " . $tbname . 
								"(`protocolid`, `studyid`, `xlcat`, `xlscat`, `xltestcd`, `xltest`,
								`xlmodify`, `xlstat`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
						VALUES ('$protocolid', '$studyid', '$xlcat', '$xlscat', '$xltestcd', '$xltest',
								'$xlmodify', '$xlstat', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ");
						
					if ($result) {
						$res['message'] = "New record added successfully $xlmodify";
						//echo $i. "New record added successfully $xlmodify";
					} else{
						$res['error'] = true;
						$res['message'] = "Insert new record failed";
						//echo $i ."Insert new record failed $xlmodify";
					}
					
					$i=$i+1;
				}
			}

			}

			fclose($handle);			
				
			return true;			
		}else{
			return false;
		}
							  
	}else{
		return false;
	} 
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>