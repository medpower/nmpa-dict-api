<?php

include_once "../config/ndp/_init_.php";

//use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 

//Initialize the action as read;
$xlcat="";
$xlscat="";
$xltype = "";
$testcode = "";
$status = "";

if (isset($_POST['action']) && !empty($_POST['action'])) {
	$action=urldecode($_POST["action"]);
}

$userid=strtoupper($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

$varlist ="*";
$limit = '';
$orderby = '';
$where_condition = 'where rmfl = "N" ';

$where_condition=$where_condition . " and (
	upper(cruserid) = '" . $userid. "' or 
	accesslevel = 1 or (
					accesslevel = 2 and 
					cruserid in (select userid from ". JWT_DBUSER . " where  upper(substring_index(email,'@',-1))='" . $company_domain . "') 
					and '" . $userid . "' in (select upper(userid) from ". JWT_DBUSER . " where isdngb='Y') 
					)
	)";

//construct the select variable list when applicable;
if (isset($_POST['varlist']) && !empty($_POST['varlist'])) {
	$varlist=str_replace(' ',',',preg_replace('/\s+/', ' ', urldecode($_POST['varlist'])));
}

//construct the where condition when applicable;
if (isset($_POST['filter']) && !empty($_POST['filter'])) {
	$filter=urldecode($_POST['filter']);
	$where_condition = $where_condition . ' and ' . $filter;
}

//construct the pagination limit when applicable;
if (isset($_POST['limit']) && !empty($_POST['limit'])) {
	$limit=' limit ' . str_replace(' ',',',preg_replace('/\s+/', ' ', urldecode($_POST['limit'])));
}

//construct the order by statement when applicable;
if (isset($_POST['byvar']) && !empty($_POST['byvar'])) {
	$orderby=' order  by ' . str_replace(' ',',',preg_replace('/\s+/', ' ', urldecode($_POST['byvar'])));
}

// $res['sql'] = $where_condition;
//For read operation;

if ($action == 'read') {

	$sql='select ' . $varlist . ' from ' . $tbname . " utf8  " . $where_condition . $orderby . $limit;
	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

	//close connection and output json object;
	$conn -> close();
	
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $sql;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

if ($action == 'read-random') {

    $keyname=urldecode($_POST["keyname"]);
    $limit=$_POST["limit"];

	$sql='select ' . $varlist . ' from ' . $tbname . " as t1 join (select " . $keyname . " from " . $tbname . 
            " order by rand() limit " . $limit . ") as t2 on t1." . $keyname . "=t2." . $keyname . " " .  $where_condition ;
	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

	//close connection and output json object;
	$conn -> close();
	
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $sql;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>