<?php

include_once "../config/_init_.php";
//use \Firebase\JWT\JWT;

cors();

chkJWT();

$host = "https://clinicaltrial.asia/api/gg/v1";
$src_text=$_POST["src"];
// $src_text="dog@@thisisforatest";

$durl = $host."?src=".$src_text;
$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0); 
curl_setopt($ch, CURLOPT_URL, $durl);
// curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
// curl_setopt($ch, CURLOPT_POSTFIELDS, $src_text);
$res = curl_exec($ch);
curl_close($ch);

header("Content-type: application/json; charset=utf-8");

echo $res;
die();

?>