<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


if (isset($_POST['strColumnList']) && !empty($_POST['strColumnList'])){
    $ColumnList=explode(" ", $_POST['strColumnList']);
}
else{
    $ColumnList=['XLTEST'];
}

if (isset($_POST['strColumnListN']) && !empty($_POST['strColumnListN'])){
    $ColumnListN=explode(" ", $_POST['strColumnListN']);
}
else{
    $ColumnListN=['0'];
}


$res = array('error' => false);

$res['debug'] = "Not available";
			
//Initialize the action as read;
$action = 'upload-csv';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}

if (isset($_POST['filename']) && !empty($_POST['filename'])) {
	$filename = $_POST['filename'];
}

// $scope="study";

// $tbname =strtolower("_xd_nmpa_". $scope . "_" . $xlscat ."_" . $xltype);

$userid=strtolower($userid);

$targetSubFolder=$userid;
$strFileUploadDate=date("Y-m");
$destination="../upload";

$destination=$destination.DIRECTORY_SEPARATOR.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR.$filename;

$handle = fopen($destination, 'r');
$ext = pathinfo($destination, PATHINFO_EXTENSION);
$filename=pathinfo($destination, PATHINFO_FILENAME);

//set the default values for all records;
$scope='_study_';
$status='INITIAL';
$studyid=strtoupper($filename);
$protocolid=strtoupper($userid);
$xltype='LABEL';

//derive the information from description accordingly;
if (isset($_POST['description']) && !empty($_POST['description'])){
    $param=explode('|', $_POST['description']);
    if(count($param)===1){
        $protocolid=strtoupper($userid);
        $studyid=$_POST['description'];
    }
    elseif(count($param)===2){        
        $protocolid=$param[0];
        $studyid=$param[1];
    }
    elseif(count($param)===3){
        $protocolid=$param[0];
        $studyid=$param[1];
        $xltype=$param[2];
    }
}
else{
    $protocolid=strtoupper($userid);
    $studyid=strtoupper($filename);
}

if (isset($_POST['removeduplicate']) && !empty($_POST['removeduplicate'])){
    $removeduplicate=$_POST['removeduplicate'];
}
else{
    $removeduplicate=0;
}

//update per request setting;
if($_POST['scope']==1){
    //global default name;
    $scope='_global_';
    //LOCKED for global uploads by default
    $status='LOCKED';
}
elseif($_POST['scope']==2)
{
    //user selected protocol and study name from database;
    $scope='_study_';
    $status='INITIAL';
    if (isset($_POST['studyid']) && !empty($_POST['studyid'])){
        $studyid=$_POST['studyid'];
    }
    if (isset($_POST['protocolid']) && !empty($_POST['protocolid'])){
        $protocolid=$_POST['protocolid'];
    }    
}
elseif($_POST['scope']==3)
{
    //user customized protocol and study name;
    $scope='_study_';
    $status='INITIAL';
}

if (isset($_POST['comment']) && !empty($_POST['comment'])) {
	$xlcomment = $_POST['comment'];
}
else{
    $xlcomment ='';
}

$xlcrdtc = RUN_DTC;
$xlmodtc = RUN_DTC;
$xlauditlog = RUN_DTC . " " . $userid . " system imported";
$xlcruser=strtoupper($userid);
$xlmouser='';


//reset the table name to be imported into;

$tbname="_xd_nmpa" . $scope . str_replace('-','_',$_POST['category']);

// $varlabel=['PROTOCOL', 'STUDY', 'PROTOCOLID', 'STUDYID', 'XLCAT', 'XLSCAT', 'XLTYPE', 'XLTEST', 'XLMODIFY','XLCOMMENT', 'XLTESTCD', 'XLTESTCD', 'XLTEST', 'XLSTAT', 
//             'CAT',   'SCAT',   'SUBCAT' , 'CATEGORY', 'SUBCATEGORY', 'TYPE',   'SOURCE', 'TARGET',  'COMMENT',   'ELEMENT',  'NAME',     'LABEL', 'STATUS'];
// $varname= ['PROTOCOLID', 'STUDYID', 'PROTOCOLID', 'STUDYID', 'XLCAT', 'XLSCAT', 'XLTYPE', 'XLTEST', 'XLMODIFY','XLCOMMENT', 'XLTESTCD', 'XLTESTCD', 'XLTEST', 'XLSTAT', 
//             'XLCAT', 'XLSCAT', 'XLSCAT' , 'XLCAT',    'XLSCAT',      'XLTYPE', 'XLTEST', 'XLMODIFY','XLCOMMENT', 'XLTESTCD', 'XLTESTCD', 'XLTEST', 'XLSTAT'];

$varstudy=['PROTOCOL', 'STUDY', 'PROTOCOLID', 'STUDYID'];
$varAudit=['AUDIT', 'XLAUDITLOG', 'XLCRUSER', 'XLMOUSER'];

$sql_varlist='';
$sql_vallist='';
$rowid=0;
$sql='';
$source_order=0;
$IfColumnWithStudy=0; 

##0 NO, 1 YES but with default, 2 YES; 

if(strtoupper($ext)=='CSV'){

    while (($data = fgetcsv($handle)) !== false) {

        // 下面这行代码可以解决中文字符乱码问题
        //$data[0] = iconv('gbk', 'utf-8', $data[0]);

        if ($data){

            $seq=0;
            if($rowid==0){             
                for ($seq=0; $seq<count($ColumnList); $seq++) {

                    if ($ColumnList[$seq]==='XLTEST'){
                        $source_order=$seq;
                    }

                    if (array_search(strtoupper($ColumnList[$seq]), $varstudy)>0){
                        $IfColumnWithStudy=1;
                    }

                    if (array_search(strtoupper($ColumnList[$seq]), $varAudit)>0){
                        $IfColumnWithStudy=2;
                    }

                    if ($seq==0){
                        $sql_varlist = $ColumnList[$seq] ;
                    }
                    else{
                        $sql_varlist = $sql_varlist . ', ' . $ColumnList[$seq] ;            
                    };

                }

                $res['rf-collist'] =$ColumnList;
                $res['rf-varlist'] =$sql_varlist;

                if ($IfColumnWithStudy===0){
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(protocolid, studyid, " . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                    
                }
                elseif ($IfColumnWithStudy===1)
                {
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(" . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                    
                }
                else
                {
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(" . $sql_varlist . ") values \n";
                }

                $rowid+=1;
                continue;
            }
           
            $sql_vallist='';

            // 转义特殊字符     

            // $sql = $sql . $sql_varlist . " values ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ); \n " ;
                   
            // $res['sql'] = $sql;

            //only add unique value;
            $res['rf0'] =$IfColumnWithStudy;
            $res['rf1'] =$removeduplicate;
            $res['rf2'] =$data[$source_order];
            $res['rf3'] =$tbname;
            $res['rf4']= "SELECT * FROM " . $tbname . " utf8 " . 				
            " where xltest = '" . $data[$source_order] . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'";

            if ($removeduplicate==1){

                $result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
                            ' where xltest = "' . $data[$source_order] . '" and protocolid = "' . $protocolid . '" and studyid = "' . $studyid . '"');
                            
                $no=$result->num_rows;		

                if ($no==0){
                       
                    for ($seq=0; $seq<count($ColumnList); $seq++) {
                        
                        if ($seq==0){
                            $sql_vallist = "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'" ;
                        }
                        else{
                            $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'";            
                        };  

                    }                   

                    if ($IfColumnWithStudy===0){
                        if ($rowid===1){
                            $sql = " ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                        }
                    }
                    elseif ($IfColumnWithStudy===1)
                    {
                        if ($rowid===1){
                            $sql = " (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                        }
                    }
                    else
                    {
                        if ($rowid===1){
                            $sql = " (" . $sql_vallist . ") \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", (" . $sql_vallist . ") \n " ;                    
                        }
                    }
                    $rowid+=1;
                }
            }
            else{

                for ($seq=0; $seq<count($ColumnList); $seq++) {
                        
                    if ($seq==0){
                        $sql_vallist = "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'" ;
                    }
                    else{
                        $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'";            
                    };  

                }      

                if ($IfColumnWithStudy===0){
                    if ($rowid===1){
                        $sql = " ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                    }
                }
                elseif ($IfColumnWithStudy===1)
                {
                    if ($rowid===1){
                        $sql = " (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                    }
                }
                else
                {
                    if ($rowid===1){
                        $sql = " (" . $sql_vallist . ") \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", (" . $sql_vallist . ") \n " ;                    
                    }
                }

                $rowid+=1;
            }
        
        }

    }

    //execute the sql when all of line added
    $sql = $sql_varlist . $sql  ;
    $result = $conn->multi_query($sql);
    // $result = true;
                    
    if ($result) {
        fclose($handle);		
        $res['message'] = "术语后台数据库添加完成！";
        $res['sql'] = $sql;
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
        //echo $i. "New record added successfully $xlmodify";
    } else{
        fclose($handle);		
        $res['error'] = true;
        $res['sql'] = $sql;
        $res['message'] = "术语后台数据库添加失败！";
        //echo $i ."Insert new record failed $xlmodify";
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
    }
}
elseif(strtoupper($ext)=='TXT')
{
    while (($data = fgetcsv($handle, 0, "\t")) !== false) {

        // 下面这行代码可以解决中文字符乱码问题
        //$data[0] = iconv('gbk', 'utf-8', $data[0]);

        if ($data){

            $seq=0;
            if($rowid==0){               
                for ($seq=0; $seq<count($ColumnList); $seq++) {

                    if ($ColumnList[$seq]==='XLTEST'){
                        $source_order=$seq;
                    }

                    if (array_search(strtoupper($ColumnList[$seq]), $varstudy)>0){
                        $IfColumnWithStudy=1;
                    }

                    if (array_search(strtoupper($ColumnList[$seq]), $varAudit)>0){
                        $IfColumnWithStudy=2;
                    }

                    if ($seq==0){
                        $sql_varlist = $ColumnList[$seq] ;
                    }
                    else{
                        $sql_varlist = $sql_varlist . ', ' . $ColumnList[$seq] ;            
                    };

                }

                if ($IfColumnWithStudy===0){
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(protocolid, studyid, " . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                    
                }
                elseif ($IfColumnWithStudy===1){
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(" . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                    
                }
                else
                {
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(" . $sql_varlist . ") values \n";
                    
                }

                $rowid+=1;
                continue;
            }
           
            $sql_vallist='';

            // 转义特殊字符     

            // $sql = $sql . $sql_varlist . " values ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ); \n " ;
                   
            // $res['sql'] = $sql;

            //only add unique value;
            $res['rf1'] =$removeduplicate;
            $res['rf2'] =$data[$source_order];
            $res['rf3'] =$tbname;
            $res['rf4']= "SELECT * FROM " . $tbname . " utf8 " . 				
            " where xltest = '" . $data[$source_order] . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'";

            if ($removeduplicate==1){

                $result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
                            ' where xltest = "' . $data[$source_order] . '" and protocolid = "' . $protocolid . '" and studyid = "' . $studyid . '"');
                            
                $no=$result->num_rows;		

                if ($no==0){

                    for ($seq=0; $seq<count($ColumnList); $seq++) {
                        
                        if ($seq==0){
                            $sql_vallist = "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'" ;
                        }
                        else{
                            $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'";            
                        };  

                    }      

                    if ($IfColumnWithStudy===0){
                        if ($rowid===1){
                            $sql = " ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                        }
                    }
                    elseif ($IfColumnWithStudy===1)
                    {
                        if ($rowid===1){
                            $sql = " (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                        }
                    }
                    else
                    {
                        if ($rowid===1){
                            $sql = " (" . $sql_vallist . ") \n " ;                   
                        }
                        else
                        {
                            $sql = $sql . ", (" . $sql_vallist . ") \n " ;                    
                        }
                    }
                    $rowid+=1;
                }
            }
            else{

                for ($seq=0; $seq<count($ColumnList); $seq++) {
                        
                    if ($seq==0){
                        $sql_vallist = "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'" ;
                    }
                    else{
                        $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'";            
                    };  

                }      

                if ($IfColumnWithStudy===0){
                    if ($rowid===1){
                        $sql = " ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                    }
                }
                elseif ($IfColumnWithStudy===1)
                {
                    if ($rowid===1){
                        $sql = " (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                    }
                }
                else
                {
                    if ($rowid===1){
                        $sql = " (" . $sql_vallist . ") \n " ;                   
                    }
                    else
                    {
                        $sql = $sql . ", (" . $sql_vallist . ") \n " ;                    
                    }
                }

                $rowid+=1;
            }
        
        }

    }

    //execute the sql when all of line added
    $sql = $sql_varlist . $sql  ;
    $result = $conn->multi_query($sql);
    // $result = true;
                    
    if ($result) {
        fclose($handle);		
        $res['message'] = "术语后台数据库添加完成！";
        $res['sql'] = $sql;
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
        //echo $i. "New record added successfully $xlmodify";
    } else{
        fclose($handle);		
        $res['error'] = true;
        $res['sql'] = $sql;
        $res['message'] = "术语后台数据库添加失败！";
        //echo $i ."Insert new record failed $xlmodify";
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
    }
}
else{
        fclose($handle);	
        $res['error'] = true;
        $res['message'] = "术语文件格式错误！";
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
};
?>
