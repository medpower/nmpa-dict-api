<?php

include_once "../config/_init_.php";

//use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'review';
$xltype = "";
$testcode = "";
$status = "";

$userid=strtoupper($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

// $where_condition="where xlrmfl <> 'Y' and (xlcruser = '" . $userid. "' or json_extract(xlreviewer, '$.uid')='" . $userid . "')";

//access control query only those items owned by current user, or public or with the same company domain when accesslevel==2;

$where_condition="where xlrmfl <> 'Y' and find_in_set('" . $userid. "', upper(xlreviewer)) ";

$varlist ="`id`,
`protocolid`,
`studyid`,
`xlcat`,
`xlscat`,
`xltest`,
`xltestcd`,
`xlmodify`,
`xlstat`,
`xlcrdtc`,
`xlmodtc`,
`xlcruser`,
`xlmouser`,
`xlauditlog`,
`xltype`,
`xlcomment`,
`accesslevel`";

// $res['sql'] = $where_condition;
//For read operation;
if ($action == 'review') {

	if(urldecode($keyvalue) !="SHOW" && urldecode($keyvalue) !="SHOW ALL"){				
		$where_condition=$where_condition . " and upper(" . $keyname . ") like '" . urldecode($keyvalue) . "%'";
	}

	$sql = "SELECT " .  $varlist . " FROM " . $tbname . " utf8 " . $where_condition . " order by xltest, xltestcd ";
	

	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

	//close connection and output json object;
	$conn -> close();
	
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $sql;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>