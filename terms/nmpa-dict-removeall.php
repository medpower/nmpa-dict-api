<?php

require_once "_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";
$where_condition=" where xlrmfl <> 'Y' and upper(xlstat) not in ('APPROVED', 'LOCKED' )";

//Reset the action when applicable;
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}


//for update operation;
if ($action == 'filtereditems') {
	
	$testname=urldecode($_POST['testname']);
	$where_condition= $where_condition . " and (xltest like '" . $testname . "'
					or xltestcd like '" . $testname . "' or 
					xlmodify like '" . $testname . "')";
	
	$result = $conn->query("delete from " . $tbname . $where_condition);

	if ($result) {
		$res['message'] = "Removed successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Remove failed";
	}
}
elseif ($action == 'allitems') {
	
	//$testname=urldecode($_POST['testname']);
	
	$result = $conn->query("delete from " . $tbname . $where_condition);

	if ($result) {
		$res['message'] = "Removed successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Remove failed";
	}	
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>