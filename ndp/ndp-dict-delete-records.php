<?php

include "../config/ndp/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

//Initialize the action as read;
$action = 'filtereditems';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}


if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}

if (isset($_POST['deltype']) && !empty($_POST['deltype'])) {
	$deltype = $_POST['deltype'];
}
else{
    $deltype = "LOGICAL";
}

$debug = 'N';

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}


//Only be able to remove the items created by the owner;



//for update operation;
if ($action == 'filtereditems') {
	
	$keylist=urldecode($_POST['keylist']);
	// $where_condition= $where_condition . " and (xltest like '" . $testname . "'
	// 				or xltestcd like '" . $testname . "' or 
	// 				xlmodify like '" . $testname . "')";
	

	if($deltype == "LOGICAL")
	{
		// $where_condition=" where upper(status) not in ('APPROVED', 'LOCKED', 'ARCHIVED' ) and upper(cruserid) = '" . strtoupper($userid). "' ";
        $where_condition=" where upper(status) not in ('APPROVED', 'LOCKED', 'ARCHIVED' ) ";
		$where_condition= $where_condition . " and ( ndp_id in (" . $keylist .  ") )";

		$rnd =random_character($length = 8);
		$run_dt=RUN_DTC;
						
		$sql="create temporary table _xd_nmpa_tmp_" . $rnd . " as 
				select ndp_id as seq from " . $tbname . " " 
				. $where_condition . ";" .  
				"update " . $tbname . ", _xd_nmpa_tmp_" . $rnd . "
				SET `modtc` = '$run_dt', mouserid = '$userid',
					`rmfl` = 'Y'					
				,`auditlog` = concat_ws('\\n', ifnull(auditlog,'----------'),'$run_dt $userid deleted logically')
				where ndp_id=seq; 
				drop temporary table _xd_nmpa_tmp_" . $rnd . ";";

				
		if ($debug=='Y'){
			$res['sql']=$sql;
		}
				
		$result = $conn->multi_query($sql);
		
		if ($result) {
			$res['message'] = "Removed successfully";
			$res['debug'] = $where_condition;
		} else{
			$res['error'] = true;
			$res['message'] = "Remove failed";
			$res['debug'] = $where_condition;
		}
	}
	// else{
	// 	$rnd =random_character($length = 8);
	// 	$run_dt=RUN_DTC;

	// 	$where_condition=" where upper(xlstat) in ('DELETED') and upper(xlcruser) = '" . strtoupper($userid). "' ";
	// 	$where_condition= $where_condition . " and ( id in (" . $keylist .  ") )";

	// 	$result = $conn->query("delete from " . $tbname . $where_condition);
		
	// 	if ($result) {
	// 		$res['message'] = "Removed successfully";
	// 		$res['debug'] = $where_condition;
	// 	} else{
	// 		$res['error'] = true;
	// 		$res['message'] = "Remove failed";
	// 		$res['debug'] = $where_condition;
	// 	}

	// }
					

}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>