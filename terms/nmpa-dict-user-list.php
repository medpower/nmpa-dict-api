<?php

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

$action = 'query';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "PUBLIC";
}

$tbname = '_xd_nmpa_user_list';

//Initialize the action as read;
$xlcat="";
$xlscat="";
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where 1>0";

$sql="SELECT department, userid, username, email from " . $tbname . " utf8  " . $where_condition . " order by userid";
$res['sql'] = $sql;

$result = $conn->query($sql);

if($result){
    $res['message'] = "User list retrieved successfully";
    $records = array();
    while ($row = $result->fetch_assoc()){
        array_push($records, $row);
    }
    $res['records'] = $records;
    $res['error'] = false;
}
else{
    $res['message'] = "User list retrieved failed";
    $res['records'] = "";
    $res['error'] = true;
}

// close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>