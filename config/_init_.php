<?php

// include_once "../func/getmac.php";

//require_once "../auth/jwt/JWK.php";
//require_once "../auth/jwt/JWT.php";
//require_once "../auth/jwt/ExpiredException.php";
//require_once "../auth/jwt/BeforeValidException.php";
//require_once "../auth/jwt/SignatureInvalidException.php";
//$run_dt = date("Y-m-d") . "T" . date("H-i-s");

define('JWT_KEY', '$*&Esd()@412!*&Esd()@4112!*&Esd())@412!*&Esd()@4112!*&Es');
define('JWT_ENV', 'DEV');

// define('JWT_SERVERNAME', '172.30.220.201');
// define('JWT_SERVERNAME', '10.128.2.135');
// define('JWT_SERVERNAME', '10.131.2.164');
// define('JWT_SERVERNAME', '10.130.3.65');

define('JWT_SERVERNAME', '172.30.227.100');

define('JWT_DBNAME', 'nmpadict');
define('JWT_USERID', 'nmpadict');
define('JWT_PASSWORD', 'NVS-4218');

define('JWT_DBUSER', 'nmpadict._xd_nmpa_user_list');
define('JWT_DBLOG', 'nmpadict._xd_nmpa_user_log');
define('JWT_DBSTUDY', 'nmpadict._xd_nmpa_study_info');
define('RUN_DTC', date("Y-m-d") . "T" . date("H-i-s"));
define('MAIL_PASSWORD','v5NB3czGB4PN6QT');

date_default_timezone_set('PRC');

if (isset($_POST['keyname']) && !empty($_POST['keyname'])) {
	$keyname = $_POST['keyname'];
}

if (isset($_POST['keyvalue']) && !empty($_POST['keyvalue'])) {
	$keyvalue = urldecode($_POST['keyvalue']);
}	

if (isset($_POST['keytablename']) && !empty($_POST['keytablename'])) {
	$keytablename = explode("-",$_POST['keytablename']);
	if (count($keytablename) ==3){
		$keyscope = $keytablename[0];
		$keycategory = $keytablename[1];
		$keytype = $keytablename[2];
		$tbname="_xd_nmpa_" . $keyscope . "_" . $keycategory . "_" . $keytype;
	}
}

if (isset($_POST['uid']) && !empty($_POST['uid'])) {
	$userid=urldecode($_POST['uid']);
}	
	

//$userToken=$_SERVER['HTTP_AUTHORIZATION'];

$headers = getallheaders();

$res = array('error' => false);
$res['debug'] = "Not available";

// $res['headers'] =$headers['Authorization'];

$jwt_token=explode(" ",urldecode($headers['Authorization']))[1];

// chkJWT();  


function encode($payload, $key, $alg = 'SHA256')
    {
        $key = md5($key);
        $jwt = urlsafeB64Encode(json_encode(['typ' => 'JWT', 'alg' => $alg])) . '.' . urlsafeB64Encode(json_encode($payload));
        return $jwt . '.' . signature($jwt, $key, $alg);
    }

function signature($input, $key, $alg='SHA256')
{
	return hash_hmac($alg, $input, $key);
}

function decode($jwt, $key)
    {
        $tokens = explode('.', $jwt);
        $key    = md5($key);

        if (count($tokens) != 3)
            return 'Invalid token';

        list($header64, $payload64, $sign) = $tokens;

        $header = json_decode(urlsafeB64Decode($header64), JSON_OBJECT_AS_ARRAY);
        if (empty($header['alg']))
            return false;

        if (signature($header64 . '.' . $payload64, $key, $header['alg']) !== $sign)
            return false;

        $payload = json_decode(urlsafeB64Decode($payload64), JSON_OBJECT_AS_ARRAY);

        return $payload;
    }

function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;

        if ($remainder)
        {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }

        return base64_decode(strtr($input, '-_', '+/'));
    }
   
   
function urlsafeB64Encode($input)
{
	return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
}

// Generates and signs a JWT for User;
function genJWT($userid) {
  // Make an array for the JWT Payload
	$time = $_SERVER['REQUEST_TIME'];
	$payload = array(
		"iss" => $_SERVER['HTTP_HOST'],
		"aud" => $_SERVER['HTTP_HOST'],
		"iat" => $time, //issue time
		"exp" => $time + 60 * 60 * 12, //expire time
		"nbf" => $time, //effective time
		"user" => $userid,
		"jti" => md5(uniqid('DATA42-JWT').$userid) //unique token identifier
	);
 
	// encode the payload using our secretkey and return the token
	return encode($payload, JWT_KEY);
}


// check token;
function chkJWT() {
  // Make an array for the JWT Payload
  global $jwt_token;
  try{
	$payload = decode($jwt_token, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	$jwt=encode($payload, JWT_KEY);
	if ($jwt != $jwt_token){
		$res['records'] = null;
		$res["error"]=true;
		$res["code"]=401;
		$res['message'] = "Unauthorized access, invalid token.";
		$res["logoff"]=true;	
		header("HTTP/1.1 200 Authentication; Content-type: application/json");
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}
	elseif($_SERVER['REQUEST_TIME'] <= $payload['nbf']-30){
		$res['records'] = null;
		$res["error"]=true;
		$res["REQUEST_TIME"]=$_SERVER['REQUEST_TIME'];
		$res["EFFECT_TIME"]=$payload['nbf'];
		$res["error"]=true;
		$res['message'] = "Service does not take effect.";	
		$res["logoff"]=true;
		header("HTTP/1.1 501 Service is not available; Content-type: application/json");
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}
	elseif($_SERVER['REQUEST_TIME'] >= $payload['exp']+30){
		$res['records'] = null;
		$res["error"]=true;
		$res['message'] = "Current session has expired.";	
		$res["logoff"]=true;
		header("HTTP/1.1 403 Authorization; Content-type: application/json");
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}
	elseif($_SERVER['REQUEST_TIME'] >= $payload['exp'] - 30 * 60){
		//renew token if to be expired in prespecified time window;
		$jwt = genJWT($userid);
		$res["error"]=false;
		$res["jwt"]=$jwt;
		$res['message'] = "Token renewed successfully.";	
		$res["logoff"]=false;
		return "renewed"; 
	}
	else{
		$res["logoff"]=false;				
		return "active"; 
	}
  }
  catch(\Exception $e){
	  return "false"; 
  }
	
}

function random_password( $length = 8 ) 
{
    $str = substr(md5(time()), 0, $length);//md5加密，time()当前时间戳
    return $str;
}

function random_character( $length = 8 ) 
{
    $str = substr(md5(time()), 0, $length);//md5加密，time()当前时间戳
    return $str;
}

//判断文件大小,up to 20MB, but also limted by the settings in APACHE; 
function checkSize($size, $maxSize=20971520){
	if ($size>$maxSize) {
		$res["message"] = '上传文件过大，请确认后再重新上传！';
		return false;
	}else{
		return true;
	}
}


//判断文件是否为运行上传类型
function checkExt($fileName, $ext = ['xls', 'xlsx', 'csv', 'txt']){ 
	if (!in_array(strtolower(pathinfo($fileName)['extension']), $ext)) {
		$res["message"] = '不支持该类型文件上传，请确认后再重新上传！';
		return false;
	}else{
		return true;
	} 
}

//上传文件错误号判断
function checkError($error){
	if ($error>0) {
		switch ($error) {
			case 1:
				$ms = '上传失败，文件超过文件最大上传限制！';
				break;
			case 2:
				$ms = '上传失败，文件超过文件最大上传限制！';
				break;
			case 3:
				$ms = '部分文件上传失败，请稍后重试！';
				break; 
			case 4:
				$ms = '上传失败，没有文件被上传！';
				break; 
			case 6:
				$ms = '上传失败，找不到临时文件夹！';
				break;
			case 7:
				$ms = '上传失败，写入失败！';
				break;
			case 8:
				$ms = '上传失败，系统错误！';
				break;     
		}
		$res["message"] = $ms;
		return false;
	}else{
		return true;
	}         
}

//检验是否通过 POST 上传
function checkPost($tmpName){
	if (is_uploaded_file($tmpName)) {
		return true;
	}else{
		$res["message"] = '非法操作，请确认后重试！';
		return false;
	}
}

//上传文件信息整理
function getFilesInfo($filesInfo){
	if (is_array($filesInfo['name'])) {
		$fileArr = [];
		$i=0;
		$filesNum = count($filesInfo['name']);
		for($i=0;$i<$filesNum;$i++){
			$fileArr[$i]=[
				'name' => $filesInfo['name'][$i],
				'type' => $filesInfo['type'][$i],
				'tmp_name' => $filesInfo['tmp_name'][$i],
				'error' => $filesInfo['error'][$i],
				'size' => $filesInfo['size'][$i]
			];
		}
		return $fileArr;
	}else{
		return false;
	}
}

function object2array(&$object) {
    $object =  json_decode( json_encode( $object),true);
    return  $object;
}

function cors() {
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept");

        exit(0);
    }
}



?>