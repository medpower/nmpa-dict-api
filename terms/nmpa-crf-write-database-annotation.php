<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


$res = array('error' => false);

$res['debug'] = "Not available";
			
//Initialize the action as read;
$action = 'upload-csv';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}

if (isset($_POST['filename']) && !empty($_POST['filename'])) {
	$filename = $_POST['filename'];
}

// $scope="study";

// $tbname =strtolower("_xd_nmpa_". $scope . "_" . $xlscat ."_" . $xltype);
$tbname="_xd_nmpa_study_crf_annotation";

$userid=strtolower($userid);
$targetSubFolder=$userid;
$strFileUploadDate=date("Y-m");
$destination="../upload";

// $destination=$destination.DIRECTORY_SEPARATOR.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR.$filename;
// $handle = fopen($destination, 'r');
// $ext = pathinfo($destination, PATHINFO_EXTENSION);
// $filename=pathinfo($destination, PATHINFO_FILENAME);

//set the default values for all records;
$status='INITIAL';
$studyid=strtoupper($filename);
$protocolid=strtoupper($userid);
$xltype='ANNOTATION';

//derive the information from description accordingly;
if (isset($_POST['description']) && !empty($_POST['description'])){
    $param=explode('|', $_POST['description']);
    if(count($param)===1){
        $protocolid=strtoupper($userid);
        $studyid=$_POST['description'];
    }
    elseif(count($param)===2){        
        $protocolid=$param[0];
        $studyid=$param[1];
    }
    elseif(count($param)===3){
        $protocolid=$param[0];
        $studyid=$param[1];
        $xltype=$param[2];
    }
}
else{
    $protocolid=strtoupper($userid);
    $studyid=strtoupper($filename);
}

if (isset($_POST['comment']) && !empty($_POST['comment'])) {
	$xlcomment = $_POST['comment'];
}
else{
    $xlcomment ='';
}

$xlcrdtc = RUN_DTC;
$xlmodtc = RUN_DTC;
$xlauditlog = RUN_DTC . " " . $userid . " system imported";
$xlcruser=strtoupper($userid);
$xlmouser='';

$sql_varlist='';
$sql_vallist='';
$rowid=0;
$sql='';
$source_order=0;
$IfColumnWithStudy=0; 

while (($data = fgetcsv($handle, 0, "\t")) !== false) {

    if ($data){

        $seq=0;
        if($rowid==0){               
                $sql_varlist = $sql_varlist . ', ' . $ColumnList[$seq] ;            

            }

            if ($IfColumnWithStudy===0){
                $sql_varlist = "INSERT INTO " . $tbname . 
                "(protocolid, studyid, " . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                
            }
            elseif ($IfColumnWithStudy===1){
                $sql_varlist = "INSERT INTO " . $tbname . 
                "(" . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) values \n";
                
            }
            else
            {
                $sql_varlist = "INSERT INTO " . $tbname . 
                "(" . $sql_varlist . ") values \n";
                
            }

            $rowid+=1;
            continue;
        }
        
        $sql_vallist='';


        //only add unique value;
        $res['rf1'] =$removeduplicate;
        $res['rf2'] =$data[$source_order];
        $res['rf3'] =$tbname;
        $res['rf4']= "SELECT * FROM " . $tbname . " utf8 " . 				
        " where xltest = '" . $data[$source_order] . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'";


            for ($seq=0; $seq<count($ColumnList); $seq++) {
                    
                if ($seq==0){
                    $sql_vallist = "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'" ;
                }
                else{
                    $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$data[$ColumnListN[$seq]]) . "'";            
                };  

            }      

            if ($IfColumnWithStudy===0){
                if ($rowid===1){
                    $sql = " ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                }
                else
                {
                    $sql = $sql . ", ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                }
            }
            elseif ($IfColumnWithStudy===1)
            {
                if ($rowid===1){
                    $sql = " (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                   
                }
                else
                {
                    $sql = $sql . ", (" . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ) \n " ;                    
                }
            }
            else
            {
                if ($rowid===1){
                    $sql = " (" . $sql_vallist . ") \n " ;                   
                }
                else
                {
                    $sql = $sql . ", (" . $sql_vallist . ") \n " ;                    
                }
            }

            $rowid+=1;
            
    }

}

//execute the sql when all of line added
$sql = $sql_varlist . $sql  ;
$result = $conn->multi_query($sql);
// $result = true;
                
if ($result) {
    fclose($handle);		
    $res['message'] = "术语后台数据库添加完成！";
    $res['sql'] = $sql;
    $conn -> close();
    header("Content-type: application/json");
    echo json_encode($res,JSON_UNESCAPED_UNICODE);
    die();
    //echo $i. "New record added successfully $xlmodify";
} else{
    fclose($handle);		
    $res['error'] = true;
    $res['sql'] = $sql;
    $res['message'] = "术语后台数据库添加失败！";
    //echo $i ."Insert new record failed $xlmodify";
    $conn -> close();
    header("Content-type: application/json");
    echo json_encode($res,JSON_UNESCAPED_UNICODE);
    die();
}

?>
