<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 

$res = array('error' => false);

$res['debug'] = "Not available";
			
//Initialize the action as read;
$action = 'upload-csv';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}

if (isset($_POST['filename']) && !empty($_POST['filename'])) {
	$filename = $_POST['filename'];
}
else{
    $userid = "public";
}

// $scope="study";

// $tbname =strtolower("_xd_nmpa_". $scope . "_" . $xlscat ."_" . $xltype);

$targetSubFolder=$userid;
$strFileUploadDate=date("Y-m");
$destination="../upload";

$destination=$destination.DIRECTORY_SEPARATOR.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR.$filename;

$handle = fopen($destination, 'r');
$ext = pathinfo($destination, PATHINFO_EXTENSION);
$filename=pathinfo($destination, PATHINFO_FILENAME);

//set the default values for all records;
$scope='_study_';
$status='INITIAL';
$studyid=strtoupper($filename);
$protocolid=strtoupper($userid);
$xltype='UNASSIGNED';

//derive the information from description accordingly;
if (isset($_POST['description']) && !empty($_POST['description'])){
    $param=explode('|', $_POST['description']);
    if(count($param)===1){
        $protocolid=strtoupper($userid);
        $studyid=$_POST['description'];
    }
    elseif(count($param)===2){        
        $protocolid=$param[0];
        $studyid=$param[1];
    }
    elseif(count($param)===3){
        $protocolid=$param[0];
        $studyid=$param[1];
        $xltype=$param[2];
    }
}
else{
    $protocolid=strtoupper($userid);
    $studyid=strtoupper($filename);
}

//update per request setting;
if($_POST['scope']==1){
    //global default name;
    $scope='_global_';
    //READY for global uploads
    $status='READY';
}
elseif($_POST['scope']==2)
{
    //user selected protocol and study name from database;
    $scope='_study_';
    $status='INITIAL';
    if (isset($_POST['studyid']) && !empty($_POST['studyid'])){
        $studyid=$_POST['studyid'];
    }
    if (isset($_POST['protocolid']) && !empty($_POST['protocolid'])){
        $protocolid=$_POST['protocolid'];
    }    
}
elseif($_POST['scope']==3)
{
    //user customized protocol and study name;
    $scope='_study_';
    $status='INITIAL';
}

if (isset($_POST['comment']) && !empty($_POST['comment'])) {
	$xlcomment = $_POST['comment'];
}
else{
    $xlcomment ='';
}

$xlcrdtc = RUN_DTC;
$xlmodtc = RUN_DTC;
$xlauditlog = RUN_DTC . " " . $userid . " system imported";
$xlcruser=$userid;
$xlmouser='';


//reset the table name to be imported into;

$tbname="_xd_nmpa" . $scope . str_replace('-','_',$_POST['category']);

//remove duplicate or not in current study level;
$uniquevalue=$_POST['removeduplicate'];

$columnHeader='ST'; //source target ST, Name Label NL, type source target TST, type name label TNL;

if(strtoupper($ext)=='CSV'){

    while (($data = fgetcsv($handle)) !== false) {
        // 下面这行代码可以解决中文字符乱码问题
        //$data[0] = iconv('gbk', 'utf-8', $data[0]);

        if ($data){
        // 跳过第一行标题
        if (strtoupper($data[0]) == 'SOURCE' || 
            strtoupper($data[0]) == 'NAME' || 
            strtoupper($data[0]) == 'TYPE' || 
            strtoupper($data[0]) == 'CAT' || 
            strtoupper($data[0]) == 'CATEGORY' || 
            strtoupper($data[0]) == 'LABEL'
            ) {
            
            if (count($data)==2){
                if(strtoupper($data[0]) == 'NAME' && 
                        (strtoupper($data[0]) == 'LABEL' || strtoupper($data[0]) == 'SOURCE')){
                    $columnHeader='NL';
                }
                elseif(strtoupper($data[0]) == 'SOURCE' && strtoupper($data[0]) == 'TARGET'){
                    $columnHeader='ST';
                }
                elseif(strtoupper($data[0]) == 'TYPE' && strtoupper($data[0]) == 'SOURCE'){
                    $columnHeader='TS';
                }
                else {
                    $columnHeader='NA';
                }
            }
            elseif(count($data)==3){
                if(strtoupper($data[0]) == 'TYPE'  && strtoupper($data[1]) == 'NAME' && strtoupper($data[2]) == 'LABEL'){
                    $columnHeader='TNL';
                }
                elseif(strtoupper($data[0]) == 'TYPE'  && strtoupper($data[1]) == 'SOURCE' && strtoupper($data[2]) == 'TARGET'){
                    $columnHeader='TST';
                }
                elseif(strtoupper($data[0]) == 'NAME'  && strtoupper($data[1]) == 'LABEL' && strtoupper($data[2]) == 'TARGET'){
                    $columnHeader='NLT';
                }
                else {
                    $columnHeader='NA';
                }
            }
            continue;
        }
        
        //$item_list[] = $data;

        // data 为每行的数据，这里转换为一维数组
        //print count($data);
        //print_r($data);// Array ( [0] => tom [1] => 12 )    
        
        if(count($data)==1) {
            //for only one column in the source file, will be regarded as SOURCE and store into XLTEST only;
            $xltestcd = '';
            $xltest = $data[0];
            $xlmodify = "";
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        elseif(count($data)==2) {
            //for two columns in the source file;
            if ($columnHeader=='NL'){
                $xltestcd = $data[0];
                $xltest = $data[1];
                $xlmodify = "";
            }
            elseif ($columnHeader=='ST'){
                $xltestcd = '';
                $xltest = $data[0];
                $xlmodify = $data[1];
            }
            elseif ($columnHeader=='TS'){
                $xltestcd = '';
                $xltest = $data[1];
                $xltype = $data[0];
                $xlmodify = "";
            }
            else{
                $xltestcd = '';
                $xltest = $data[0];
                $xlmodify = $data[1];
            }
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        elseif(count($data)==3) {
            //for three columns in the source file, like TYPE NAME LABEL;

            if ($columnHeader=='TNL'){
                $xltype = $data[0];
                $xltestcd = $data[1];
                $xltest = $data[2];
                $xlmodify = "";
            }
            elseif ($columnHeader=='TST'){
                $xltype = $data[0];
                $xltestcd = '';
                $xltest = $data[1];
                $xlmodify = $data[2];
            }
            elseif ($columnHeader=='NLT'){
                $xltestcd = $data[0];
                $xltest = $data[1];           
                $xlmodify = $data[2];
            }
            else{
                $xltype = $data[0];
                $xltestcd = $data[1];
                $xltest = $data[2];
                $xlmodify = '';            
            }
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        elseif(count($data)==4) {
            //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
            $xltype = $data[0];
            $xltestcd = $data[1];
            $xltest = $data[2];
            $xlmodify = $data[3];
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        elseif(count($data)==5) {
            //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
            $xltype = $data[0];
            $xltestcd = $data[1];
            $xltest = $data[2];
            $xlmodify = $data[3];
            $xlcomment = $data[4];
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcomment`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcomment',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        elseif(count($data)==6) {
            //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
            $xltype = $data[0];
            $xlcat = $data[1];
            $xlscat = $data[2];
            $xltestcd = $data[3];
            $xltest = $data[4];
            $xlmodify = $data[5];
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcat`,`xlscat`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcat','$xlscat',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }
        else{
            $xltype = $data[0];
            $xlcat = $data[1];
            $xlscat = $data[2];
            $xltestcd = $data[3];
            $xltest = $data[4];
            $xlmodify = $data[5];
            $xlcomment = $data[6];
            $sql = "INSERT INTO " . $tbname . 
            "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcat`,`xlscat`,`xlcomment`,
            `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
    VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcat','$xlscat','$xlcomment',
            '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
        }

        //only add unique value;
        $result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
                    " where xltest = '" . $xltest . "' and xlmodify = '" . $xlmodify . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'");
                    
        $no=$result->num_rows;		

        if ($no==0){

            $result = $conn->query($sql);
                
            if ($result) {
                $res['message'] = "New record added successfully $xlmodify";
                //echo $i. "New record added successfully $xlmodify";
            } else{
                $res['error'] = true;
                $res['message'] = "Insert new record failed";
                //echo $i ."Insert new record failed $xlmodify";
            }
            
            $i=$i+1;
        }
    }

    }
}
else
{
    while (($data = fgetcsv($handle, 0, "\t")) !== false) {
        // 下面这行代码可以解决中文字符乱码问题
        //$data[0] = iconv('gbk', 'utf-8', $data[0]);

        if ($data){
            // 跳过第一行标题
            if (strtoupper($data[0]) == 'SOURCE' || 
                strtoupper($data[0]) == 'NAME' || 
                strtoupper($data[0]) == 'TYPE' || 
                strtoupper($data[0]) == 'CAT' || 
                strtoupper($data[0]) == 'CATEGORY' || 
                strtoupper($data[0]) == 'LABEL'
                ) {
                
                if (count($data)==2){
                    if(strtoupper($data[0]) == 'NAME' && 
                            (strtoupper($data[0]) == 'LABEL' || strtoupper($data[0]) == 'SOURCE')){
                        $columnHeader='NL';
                    }
                    elseif(strtoupper($data[0]) == 'SOURCE' && strtoupper($data[0]) == 'TARGET'){
                        $columnHeader='ST';
                    }
                    elseif(strtoupper($data[0]) == 'TYPE' && strtoupper($data[0]) == 'SOURCE'){
                        $columnHeader='TS';
                    }
                    else {
                        $columnHeader='NA';
                    }
                }
                elseif(count($data)==3){
                    if(strtoupper($data[0]) == 'TYPE'  && strtoupper($data[1]) == 'NAME' && strtoupper($data[2]) == 'LABEL'){
                        $columnHeader='TNL';
                    }
                    elseif(strtoupper($data[0]) == 'TYPE'  && strtoupper($data[1]) == 'SOURCE' && strtoupper($data[2]) == 'TARGET'){
                        $columnHeader='TST';
                    }
                    elseif(strtoupper($data[0]) == 'NAME'  && strtoupper($data[1]) == 'LABEL' && strtoupper($data[2]) == 'TARGET'){
                        $columnHeader='NLT';
                    }
                    else {
                        $columnHeader='NA';
                    }
                }
                continue;
            }
            
            //$item_list[] = $data;
    
            // data 为每行的数据，这里转换为一维数组
            //print count($data);
            //print_r($data);// Array ( [0] => tom [1] => 12 )    
            
            if(count($data)==1) {
                //for only one column in the source file, will be regarded as SOURCE and store into XLTEST only;
                $xltestcd = '';
                $xltest = $data[0];
                $xlmodify = "";
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            elseif(count($data)==2) {
                //for two columns in the source file;
                if ($columnHeader=='NL'){
                    $xltestcd = $data[0];
                    $xltest = $data[1];
                    $xlmodify = "";
                }
                elseif ($columnHeader=='ST'){
                    $xltestcd = '';
                    $xltest = $data[0];
                    $xlmodify = $data[1];
                }
                elseif ($columnHeader=='TS'){
                    $xltestcd = '';
                    $xltest = $data[1];
                    $xltype = $data[0];
                    $xlmodify = "";
                }
                else{
                    $xltestcd = '';
                    $xltest = $data[0];
                    $xlmodify = $data[1];
                }
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            elseif(count($data)==3) {
                //for three columns in the source file, like TYPE NAME LABEL;
    
                if ($columnHeader=='TNL'){
                    $xltype = $data[0];
                    $xltestcd = $data[1];
                    $xltest = $data[2];
                    $xlmodify = "";
                }
                elseif ($columnHeader=='TST'){
                    $xltype = $data[0];
                    $xltestcd = '';
                    $xltest = $data[1];
                    $xlmodify = $data[2];
                }
                elseif ($columnHeader=='NLT'){
                    $xltestcd = $data[0];
                    $xltest = $data[1];           
                    $xlmodify = $data[2];
                }
                else{
                    $xltype = $data[0];
                    $xltestcd = $data[1];
                    $xltest = $data[2];
                    $xlmodify = '';            
                }
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            elseif(count($data)==4) {
                //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
                $xltype = $data[0];
                $xltestcd = $data[1];
                $xltest = $data[2];
                $xlmodify = $data[3];
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            elseif(count($data)==5) {
                //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
                $xltype = $data[0];
                $xltestcd = $data[1];
                $xltest = $data[2];
                $xlmodify = $data[3];
                $xlcomment = $data[4];
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcomment`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcomment',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            elseif(count($data)==6) {
                //for 4 columns in the source file, like TYPE, NAME LABEL, TARGET;
                $xltype = $data[0];
                $xlcat = $data[1];
                $xlscat = $data[2];
                $xltestcd = $data[3];
                $xltest = $data[4];
                $xlmodify = $data[5];
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcat`,`xlscat`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcat','$xlscat',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
            else{
                $xltype = $data[0];
                $xlcat = $data[1];
                $xlscat = $data[2];
                $xltestcd = $data[3];
                $xltest = $data[4];
                $xlmodify = $data[5];
                $xlcomment = $data[6];
                $sql = "INSERT INTO " . $tbname . 
                "(`protocolid`, `studyid`, `xltestcd`, `xltest`,`xlcruser`,`xlmouser`,`xlstat`,`xlcat`,`xlscat`,`xlcomment`,
                `xlmodify`, `xltype`, `xlcrdtc`, `xlmodtc`,`xlauditlog`) 
        VALUES ('$protocolid', '$studyid', '$xltestcd', '$xltest','$xlcruser','$xlmouser','$status','$xlcat','$xlscat','$xlcomment',
                '$xlmodify', '$xltype', '$xlcrdtc', '$xlmodtc', '$xlauditlog') ";
            }
    
            //only add unique value;
            $result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
                        " where xltest = '" . $xltest . "' and xlmodify = '" . $xlmodify . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'");
                        
            $no=$result->num_rows;		
    
            if ($no==0){
    
                $result = $conn->query($sql);
                    
                if ($result) {
                    $res['message'] = "New record added successfully $xlmodify";
                    //echo $i. "New record added successfully $xlmodify";
                } else{
                    $res['error'] = true;
                    $res['message'] = "Insert new record failed";
                    //echo $i ."Insert new record failed $xlmodify";
                }
                
                $i=$i+1;
            }
        }

    }
}
fclose($handle);			
    
$res['error'] = false;
$res['message'] = "Upload successfully";	
$res['debug'] = $tbname;				

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>