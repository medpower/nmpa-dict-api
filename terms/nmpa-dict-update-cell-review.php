<?php

include "../config/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$action = $_POST['action'];
$xlmodify = $_POST['xlmodify'];
$xlcomment = $_POST['xlcomment'];
$xlauditlog=$_POST['xlauditlog'];
$where_condition="where xlrmfl <> 'Y' ";

//for cell update operation;
if ($action == 'cell-update') {
	$id = $_POST['id'];	
	$xlmodtc = RUN_DTC;
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated translation from ' . $_POST['original-term'] . '===>' . $xlmodify . ' with comment ' . $xlcomment;
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlmodify` = '$xlmodify', `xlcomment` = '$xlcomment',
						`xlstat` = 'READY',
						a.xleval = 'MANUAL',
						`xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Cell updated successfully";
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "Cell updated failed";
		$res['debug'] = "Not applicable";
        $res['auditlog']=$xlauditlog;
	}
}


//for cell update operation;
if ($action == 'dict-update') {
	$id = $_POST['id'];	
	$xlmodtc = RUN_DTC;
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' dictionary translated from ' . $_POST['original-term'] . '===>' . $xlmodify;
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlmodify` = '$xlmodify',
						`xlstat` = 'LOCKED',
						a.xleval = 'DICT',
						`xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Cell updated with dictionary translation successfully";
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "Cell updated with dictionary translation failed";
		$res['debug'] = "Not applicable";
        $res['auditlog']=$xlauditlog;
	}
}

//for cell update operation;
if ($action == 'api-update') {
	$id = $_POST['id'];	
	$xlmodtc = RUN_DTC;
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' api translated from ' . $_POST['original-term'] . '===>' . $xlmodify;
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlmodify` = '$xlmodify',
						`xlstat` = 'API',
						a.xleval = 'API',
						`xlmodtc` = '$xlmodtc', `xlauditlog` = '$xlauditlog'
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	if ($result) {
		$res['message'] = "Cell updated with API translation successfully";
        $res['auditlog']=$xlauditlog;

	} else{
		$res['error'] = true;
		$res['message'] = "Cell updated with API translation failed";
		$res['debug'] = "Not applicable";
        $res['auditlog']=$xlauditlog;
	}
}


$res['debug'] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>