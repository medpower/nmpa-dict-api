<?php

	require_once "_init_.php";

	cors();
	chkJWT();

	//receive the filters when it's applied
	$res = array('error' => true);

	//Initialize the action as read;
	$testname = '';

	$where_condition=" where xlrmfl <> 'Y' and xlstat in ('Active') ";

	//Reset the action when applicable;
	if (isset($_GET['testname']) && !empty($_GET['testname'])) {
		$testname=urldecode($_GET['testname']);
		$where_condition= $where_condition . " and (xltest like '" . $testname . "')";					
	}

	// Create connection
	$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
	$conn->set_charset("utf8");
	$host = "https://api.niutrans.com";
	$path = "/NiuTransServer/translation";
	$apikey = "eb65376772d4b6ee195a8a8e23a582a1";
	$dictNo = "eed8d9d5d4";
	$memoryNo="71c53902f8";
	
	// Check connection
	if ($conn->connect_error) {
		die("Database connection established Failed..");
	} 
	$res = array('error' => false);

	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query("SELECT id, xltest  FROM " . $tbname . " utf8 " . $where_condition . " ");
	
	$records = array();
	
	$len=0;
	$src_text="";
	$seq=1;
	while ($row = $result->fetch_assoc()){
		
		//array_push($records, $row);

		$len = $len + strlen($row['xltest']);	
		
		//echo strlen($row['xltest']) . "</br>";
		if ($len >= 500){						
	
			//echo "No. $seq :" . $src_text;		
			//echo $len . "</br>";				
			//echo "-----------------------------------------" . "</br>";
			
			//process translation;

			//$src_text=$_POST["$src_text"];
			//$durl = $host.$path."?from=en&to=zh&apikey=".$apikey."&dictNo=".$dictNo."&memoryNo=".$memoryNo;

			$durl = $host.$path."?from=en&to=zh&apikey=".$apikey."&dictNo=".$dictNo."&memoryNo=".$memoryNo;;
			
			//echo $durl;
			
			$source=explode("###",$src_text);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $durl);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'src_text='.urlencode($src_text));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$data = curl_exec($ch);
			curl_close($ch);
			
			//ob_start();
			//var_dump($data);
			//$str=ob_get_clean();
						
			//echo json_encode($data,JSON_UNESCAPED_UNICODE);
			$json2Array = json_decode($data,true);
			
			//print_r($json2Array);
			
			foreach($json2Array as $key=>$val) {
				if ($key == 'tgt_text'){
					
					//echo $val . "<br/>";
					
					$target=explode("###",$val);
					
					$order=0;
					
					foreach($target as $value){
						
						echo $value . "==>" .$source[$order]  . "<br/>";
						
						$order+=1;		

						//update the database accordingly;
							$result = $conn->query("UPDATE " . $tbname . " 
							SET `xlmodify` = '$value',
								`xlstat` = 'Machine Translated', `xlmodtc` = '$run_dt'
							WHERE `xltest` = '$source[$order]' and `xlrmfl` <> 'Y'
							");
			
							if ($result) {
								$res['error'] = false;
								$res['message'] = "Translated successfully";
							} else{
								$res['error'] = true;
								$res['message'] = "Translated failed";
							}
			
					}						
				}		
			}
			
			
			//echo $data;
			
			//array_push($records, $data);
			
			//update database;
	
			
			//echo $data;
			
			//reset the loop;			
			$src_text="";
			$len=0;
			$seq+=1;
		}
		else{
			
			$src_text=$src_text. "###" . str_replace("#","#",$row['xltest']);		
			
			//echo $src_text;
		}	
		
		//echo $src_text;
		
	}
	
	//$res['records'] = $records;
	
	
	//var_dump($data);
		
	//header("Content-type: application/json");
	//echo json_encode($res,JSON_UNESCAPED_UNICODE);
	//die();
?>