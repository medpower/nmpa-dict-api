<?php

include_once "../config/_init_.php";

cors();

// if (isset($_POST['upassword']===false) 
// 	|| isset($_POST['department']===false)
// 	|| isset($_POST['email']===false)
// 	|| isset($_POST['country']===false)
// 	|| empty($_POST['upassword']) 
// 	|| empty($_POST['department']) 
// 	|| empty($_POST['email']) 
// 	|| empty($_POST['country'])
// 	){
// 	$res["error"]=true;
// 	$res["message"]="Authorized failed, invalid user id or password";
// 	exit(json_encode($res,JSON_UNESCAPED_UNICODE));	
// }

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 

//update user log file

$macaddress='';

$userid=strtolower($_POST["uid"]);

$result = $conn->query("INSERT INTO " . JWT_DBLOG . 
"(userid, accessip, accessdtc, accessmac) 
VALUES ('$userid', '" . $_SERVER["REMOTE_ADDR"] . "', '" . RUN_DTC . "', '$macaddress') ");

//query the login user if registered in the user table
	//query the login user credentials

$where_condition=" userid='" . $userid . "'" ;
$result = $conn->query("SELECT userid, email, username, isadmin FROM " . JWT_DBUSER . " utf8 where " . $where_condition . " ");
$no=$result->num_rows;

// $res['sql'] = $sql;
$isNVSUserEmail=true;

if(strtoupper(explode('@', $_POST["email"])[1])=='NOVARTIS.COM'){
	$isNVSUserEmail=true;
}

if ($no==0 && $isNVSUserEmail==true){

	$sql = "INSERT INTO " . JWT_DBUSER . 
		"(userid, username, uuid, title, division, function, department, group_lev1, group_lev2, country_name, country_code, company, address, phone, cost_center, email, regip, employeeid, hire_dtc, opmid, regdate) 
		VALUES ('$userid', '" . 
				$_POST["username"] . "','" . 
				$_POST["uuid"] . "','" . 
				$_POST["title"] . "','" . 
				$_POST["division"] . "','" . 
				$_POST["function"] . "','" . 
				$_POST["department"] . "','" . 
				$_POST["group_lev1"] . "','" . 
				$_POST["group_lev2"] . "','" . 
				$_POST["country_name"] . "','" . 
				$_POST["country_code"] . "','" . 
				$_POST["company"] . "','" . 
				$_POST["address"] . "','" . 
				$_POST["phone"] . "','" . 
				$_POST["cost_center"] . "','" . 
				$_POST["email"] . "','" . 
				$_SERVER["REMOTE_ADDR"] . "', '" . 
				$_POST["employeeid"] . "', '" . 
				$_POST["hire_dtc"] . "', '" . 
				$_POST["opmid"] . "', '" . 
				RUN_DTC . "') ";

	//REGISTER IN THE USER TABLE
	$result = $conn->query($sql);

	$res["sql"]=$sql;
	$res["alevel"]=0;
	$res["uid"]=$userid;
	$res["email"]=$_POST["email"] ;
	$res["username"]=$_POST["username"];
	$res["token"]=genJWT($userid);

}
elseif ($no==1){
	$user = array();
	while ($row = $result->fetch_assoc()){
		array_push($user, $row);
	}

	$res["alevel"]=$user[0]['isadmin'] ;//role level, admin or not
	$res["email"]=$user[0]['email'] ;
	$res["username"]=$user[0]["username"];
	$res["uid"]=$userid;
	$res["error"]=false;
	$res["message"]="Authorized successfully";
	$res["token"]=genJWT($userid);
}
else{
	$res["error"]=true;
	$res["debug"]=strtoupper(explode('@', $_POST["email"])[1]);
	$res["message"]="Authorized failed, invalid user id or password";
	$res["flag"]=$isNVSUserEmail;
	$res["no"]=$no;

	// exit(json_encode($res,JSON_UNESCAPED_UNICODE));	
}

//close connection and output json object;
$conn -> close();

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>