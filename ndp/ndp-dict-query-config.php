<?php

include_once "../config/ndp/_init_.php";

// use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 

//Initialize the action as read;

$action = 'read';

$userid=strtolower($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

$where_condition = 'where 1>0 ';


//construct the where condition when applicable;
if (isset($_POST['filter']) && !empty($_POST['filter'])) {
	$filter=urldecode($_POST['filter']);
	$where_condition = $where_condition . ' and ' . $filter;
}

//construct the order by statement when applicable;
if (isset($_POST['byvar']) && !empty($_POST['byvar'])) {
	$byvar=str_replace(' ',',',urldecode($_POST['byvar']));
	$sql='select * from ' . $tbname . " utf8  " . $where_condition . "  order by " . $byvar;
}
else{
	$sql='select * from ' . $tbname . " utf8  " . $where_condition;
}

//For read operation;
if ($action == 'read') {
	
	// $res['sql'] = $where_condition;

	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

    //close connection and output json object;
	$conn -> close();
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $where_condition;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>