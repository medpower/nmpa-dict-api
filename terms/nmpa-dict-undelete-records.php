<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;

$action = 'approveall';
$run_dt = RUN_DTC;
$where_condition=" where upper(xlstat) in ('DELETED') ";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid'])) {
	$userid = $_POST['userid'];
}

if (isset($_POST['keylist']) && !empty($_POST['keylist'])) {
	$keylist=urldecode($_POST['keylist']);
    $where_condition = $where_condition . " and a.id in (" . $keylist .  ")";
}
else{
    $keylist = "";
    $where_condition= "0 > 1";
}

//for update operation;
if ($action == 'filtereditems') {
	
	$sql="UPDATE " . $tbname . " a 
	SET a.xlstat = 'ACTIVE', a.xlrmfl = 'N', a.xlmodtc = '$run_dt', a.xlmouser = '$userid',
	a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid batch recovered.')
	" . $where_condition;
	$result = $conn->query($sql);

	// $res["sql"] = $sql;

	if ($result) {
		$res['message'] = "Recovered successfully";
	} else{
		$res['error'] = true;
		$res['message'] = "Recovered failed";
	}
}

$res["debug"] = "Not applicable";

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>