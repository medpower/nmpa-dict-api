<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$action = 'update';
$xlauditlog=$_POST['original-auditlog'];
$run_dt=RUN_DTC;

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = strtolower($_POST['action']);
}

$userid =strtoupper($_POST['userid']);
$passcode = md5($userid . $_POST['passcode']);

$tbname = '_xd_nmpa_user_list';

//for update operation;
if ($action == 'update-passcode') {

	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated passcode';

	$sql = "UPDATE " . $tbname . " 
	SET `passcode` = '$passcode',
	`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
	WHERE upper(`userid`) = '" . $userid . "'
	";
	$result = $conn->query($sql);
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Approved', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "PIN code updated successfully";
		$res['error'] = false;
		$res['debug'] = "NA";
		// $res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "PIN code updated failed";
		$res['debug'] = "$sql";		
	}
}


if ($action == 'delete') {
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' added deleted flag';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' added deleted flag';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'DELETED', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Deleted', `xlmodtc` = '$run_dt',
	// 				`xlrmfl` = 'Y'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Deleted successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Deleted failed";
	}
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>