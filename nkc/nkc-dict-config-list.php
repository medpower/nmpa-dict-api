<?php

include "../config/nkc/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

$action = 'query';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

//Initialize the action as read;

$where_condition="where active='Y' ";

$sql="SELECT category, seq, code, codename from " . $tbname . " utf8  " . $where_condition . " order by category, seq, code, codename";
$res['sql'] = $sql;

$result = $conn->query($sql);

if($result){
    $res['message'] = "Codelist retrieved successfully";
    $records = array();
    while ($row = $result->fetch_assoc()){
        array_push($records, $row);
    }
    $res['records'] = $records;
    $res['error'] = false;
}
else{
    $res['message'] = "Codelist retrieved failed";
    $res['records'] = "";
    $res['error'] = true;
}

// close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>