<?php

include_once "../config/_init_.php";
//use \Firebase\JWT\JWT;

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    die("Database connection established Failed..");
} 

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}

$where_condition=" userid='" . $userid . "'" ;
$result = $conn->query("SELECT userid, isadmin FROM " . JWT_DBUSER . " utf8 where " . $where_condition . " ");
$no=$result->num_rows;

$user = array();

while ($row = $result->fetch_assoc()){
    array_push($user, $row);
}

$res["sql"]="SELECT userid, isadmin FROM " . JWT_DBUSER . " utf8 where " . $where_condition . " ";
$res["alevel"]=$user[0]['isadmin'] ;//role level, admin or not
$conn -> close();

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>