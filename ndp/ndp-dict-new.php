<?php

include "../config/ndp/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);
$userid = $_POST['userid'];

//Reset the action when applicable;

if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

$debug = 'N';

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}

if (isset($_POST['keytablename']) && !empty($_POST['keytablename'])) {
	$tbname=str_replace("-", "_", urldecode($_POST["keytablename"]));
}

$run_dt = RUN_DTC;

$column_type_sql="SELECT column_name,data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . JWT_DBNAME . "' AND TABLE_NAME = '$tbname';";
$column_type_result = $conn->query($column_type_sql);

$columns = array();
$types = array();

while ($row = $column_type_result->fetch_assoc()){
	array_push($columns, $row["column_name"]);
	array_push($types, $row["data_type"]);
}	

$updated_varlist="";

if (isset($_POST['varlist']) && !empty($_POST['varlist'])) {
	$updated_varlist=explode(' ', urldecode($_POST['varlist']));
}

//For creation operation;
if ($action == 'create') {

    $crdtc = RUN_DTC;
    $modtc = RUN_DTC;
    $auditlog = RUN_DTC . " " . $userid . " added";

    $sql_varlist ="";
    $sql_vallist ="";
    $seq=0;

	foreach ( $updated_varlist as $var) { 

		//determine the field type;
		$type= $types[array_search($var, $columns)];

		if ($type=='char' || $type=='text' || $type =='varchar'){
			$vartype='C';
		}
		else{
			$vartype='N';
		}

        $value=urldecode($_POST[$var]);

        if ($seq==0){
            $sql_varlist = $var;
            if($vartype=='C'){
                $sql_vallist = "'" . mysqli_real_escape_string($conn,$value) . "'" ;
            }
            else{
                $sql_vallist = mysqli_real_escape_string($conn,$value);
            }
        }
        else{
            $sql_varlist = $sql_varlist . ', ' . $var;         
              
            if($vartype=='C'){
                $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$value) . "'";     
            }
            else{
                $sql_vallist = $sql_vallist . ', ' .  mysqli_real_escape_string($conn,$value) ;     
            }
        };                
        $seq+=1;

	} 
	
    //derive the sql variable list;
    $sql_varlist = "INSERT INTO " . $tbname . 
    "(ndp_uuid, " . $sql_varlist . ", cruserid, mouserid, crdtc, modtc, auditlog) \n";

    $sql = $sql_varlist . 'values ( uuid(), ' .  $sql_vallist . ", '$userid', '$userid', '$crdtc', '$modtc', '$auditlog' ); \n ";

    if ($debug=='Y'){
        $res['sql']=$sql;
    }

    $result = $conn->query($sql);
            
        if ($result) {
            $res['message'] = "New record added successfully!";
        } else{
            $res['error'] = true;
            $res['message'] = "New record added failed!";
        }


}
elseif ($action == 'rowcopy') {
	$protocolid = trim($_POST['protocolid']);
	$studyid = trim($_POST['studyid']);
	$xlcat = trim($_POST['xlcat']);
	$xlscat = trim($_POST['xlscat']);
	$xltest = trim($_POST['xltest']);
	$xltestcd = trim($_POST['xltestcd']);
	$xlmodify = trim($_POST['xlmodify']) . "(Copy)";
	$xlstat = "Active";
	$xlrmfl = "N";
	$xlcrdtc = $run_dt;
	$xlmodtc = $run_dt;
	$xlcomment = trim($_POST['xlcomment']);
	$xlauditlog=$run_dt . " created with a copy of existing item " . trim($_POST['xltest']);
	
	$result = $conn->query("INSERT INTO " . $tbname . 
				"(`protocolid`, `studyid`,`xlcat`, `xlscat`, `xltest`, `xlmodify`,
				`xltestcd`, `xlstat`, `xlrmfl`,`xlcrdtc`,
				`xlmodtc`, `xlcomment`, `xlauditlog`) 
		VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltest', '$xlmodify',
				'$xltestcd', '$xlstat', '$xlrmfl','$xlcrdtc',
				'$xlmodtc', '$xlcomment', '$xlauditlog') ");
		
	if ($result) {
		$res['message'] = "术语添加成功！";
	} else{
		$res['error'] = true;
		$res['message'] = "术语添加失败！";
	}
}
elseif ($action == 'append') {
	$tbname = "_xd_nmpa_global_" . explode('-',$_POST['keytablename'])[1] . '_' . explode('-',$_POST['keytablename'])[2];
	$protocolid = urldecode($_POST['protocolid']);
	$studyid = urldecode($_POST['studyid']);
	$xlcat = urldecode($_POST['xlcat']);
	$xlscat = urldecode($_POST['xlscat']);
	$xltest = urldecode($_POST['xltest']);
	$xltestcd = urldecode($_POST['xltestcd']);
	$xlmodify = urldecode($_POST['xlmodify']) ;
	$xlstat = "PENDING";
	$xlrmfl = "N";
	$xlcrdtc = RUN_DTC;
	$xlmodtc = '';
	$xlcomment = urldecode($_POST['xlcomment']);
	$userid = $_POST['userid'];
	$xlauditlog=$run_dt . " " . $_POST['userid'] ." appended";

	//determine whether it already exists or not;
	$sql="select id from " . $tbname . 
	" where xltest = '" . $xltest . "' and xlrmfl <> 'Y'";
	$result = $conn->query($sql);
	$num    = $result -> num_rows;  
	if ($num > 0 ){
		$res['error'] = true;
		$res['sql'] = $sql;
		$res['count'] = $num;
		$res['message'] = "术语已存在，入库操作已取消！";		
	}
	else{	
		$sql = "INSERT INTO " . $tbname . 
		"(`protocolid`, `studyid`,`xlcat`, `xlscat`, `xltest`, `xlmodify`,`xlcruser`,
		`xltestcd`, `xlstat`, `xlrmfl`,`xlcrdtc`,
		`xlmodtc`, `xlcomment`, `xlauditlog`) 
	VALUES ('$protocolid', '$studyid','$xlcat', '$xlscat', '$xltest', '$xlmodify','$userid',
		'$xltestcd', '$xlstat', '$xlrmfl','$xlcrdtc',
		'$xlmodtc', '$xlcomment', '$xlauditlog') ";

		$result = $conn->query($sql);


		$res['sql'] = $sql;

		if ($result) {
			$res['error'] = false;
			$res['message'] = "术语入库成功！";
		} else{
			$res['error'] = true;
			$res['message'] = "术语入库失败！";
		}
	}
}
else{
	$res['error'] = true;
	$res['message'] = "无效操作，请确认！";
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>