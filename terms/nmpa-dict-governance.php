<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";
$where_condition="where xlrmfl <> 'Y' ";
$xlauditlog=$_POST['original-auditlog'];
$run_dt=RUN_DTC;

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = strtolower($_POST['action']);
}

$id = $_POST['id'];
$userid = $_POST['userid'];

//for update operation;
if ($action == 'approve') {

	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to APPROVED';
	$auditlog=RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to APPROVED';
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'APPROVED', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Approved', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Approved successfully";
		$res['error'] = false;
		$res['debug'] = "Not applicable";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Approved failed";
		$res['debug'] = "Not applicable";		
	}
}

//for global pending terms lock purpose;
if ($action == 'lockg') {	
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'LOCKED', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Locked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Locked failed";
	}
}

//for study level terms lock purpose;
if ($action == 'lock') {	
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status from ' . $_POST['original-status'] . ' to LOCKED';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'LOCKED', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Locked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Locked failed";
	}
}

if ($action == 'unlock') {	
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from LOCKED to ACTIVE';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status from LOCKED to ACTIVE';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'ACTIVE', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Locked', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Unlocked successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Unlocked failed";
	}
}

if ($action == 'pending') {

	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to PENDING for inputs';
	$auditlog = RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to PENDING for inputs';
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'PENDING', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Pending for inputs', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Status updated successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Status updated failed";
	}
}

if ($action == 'on-hold') {
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to ON-HOLDE for discussion';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' updated status from ' .  $_POST['original-status'] . ' to ON-HOLDE for discussion';
	
	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'ON-HOLD', `xlmodtc` = '$run_dt'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE `id` = '$id' and `xlrmfl` <> 'Y'
					");

	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Need discussion', `xlmodtc` = '$run_dt'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Status updated successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Status updated failed";
	}
}

if ($action == 'delete') {
	
	$xlauditlog =$xlauditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' added deleted flag';
	$auditlog =RUN_DTC . " " . $_POST['userid'] . ' added deleted flag';

	$result = $conn->query("UPDATE " . $tbname . " 
					SET `xlstat` = 'DELETED', `xlmodtc` = '$run_dt',
					xlrmfl` = 'Y'
					,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$auditlog')
					WHERE upper(xlstat) not in ('APPROVED', 'LOCKED' ) and `id` = '$id' and `xlrmfl` <> 'Y' and upper(xlcruser) = '" . strtoupper($userid). "' 
					");
	
	// $result = $conn->query("UPDATE " . $tbname . " 
	// 				SET `xlstat` = 'Deleted', `xlmodtc` = '$run_dt',
	// 				`xlrmfl` = 'Y'
	// 				,`xlauditlog` = '$xlauditlog'
	// 				WHERE `id` = '$id' and `xlrmfl` <> 'Y'
	// 				");

	if ($result) {
		$res['message'] = "Deleted successfully";
		$res['auditlog']=$xlauditlog;
	} else{
		$res['error'] = true;
		$res['message'] = "Deleted failed";
	}
}

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>