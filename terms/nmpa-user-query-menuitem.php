<?php

include_once "../config/_init_.php";

// use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


//Initialize the action as read;

$action = 'read';

$userid=strtoupper($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);
$tbname = '_xd_nmpa_user_list';

$keyname=strtoupper($_POST["keyname"]);
$keyvalue=urldecode(strtoupper($_POST["keyvalue"]));

if($keyname=='OPMID' && $keyvalue !='ALL'){
	$where_condition = ' upper(opmid)="' . $keyvalue . '"';
}
elseif($keyname=='DEPARTMENT' && $keyvalue !='ALL'){
	$where_condition = ' upper(DEPARTMENT)="' . $keyvalue . '"';
}
else{
	$where_condition = '1>0';
};

$varlist = '
`uid`,
`username`,
`alias`,
`userid`,
`employeeid`,
`username_cn`,
`cellphone`,
`phone`,
`email`,
`address`,
`country_code`,
`department`,
`contact`,
`hire_dtc`,
`regdate`,
`contact_phone`,
`xlcomment`,
`xlstat`,
`opmid`,
`xltype`,
`title`,
`xlmouser`,
`xlmodtc`,
`xlauditlog`

';


$sql='select ' .  $varlist . ' from ' . $tbname . " utf8  where " . $where_condition . " order by userid ";

//For read operation;
if ($action == 'read') {
	
	// $res['sql'] = $where_condition;

	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

    //close connection and output json object;
	$conn -> close();
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	// $res['debug'] = $where_condition;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;

}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>