<?php

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}


if (isset($_POST['keylist']) && !empty($_POST['keylist']) ) {
	$keylist=urldecode($_POST['keylist']);
    $where_condition= " a.id in (" . $keylist .  ")";
}
else{
    $keylist = "";
    $where_condition= "1 > 0";
}

$dlm = urldecode($_POST['dlm']);
$res['keylist']=urldecode($_POST['keylist']);
$source_text=explode($dlm, urldecode($_POST['src_txt']));
$target_text=explode($dlm, urldecode($_POST['tgt_txt']));
                
$source_no=count($source_text);
$target_no=count($target_text);

$jobid =random_character($length = 8);
$run_dt=RUN_DTC;

if($source_no == $target_no){
    
    $tbname_temp = "_xd_nmpa_study_temp_term";

    //insert into temporary table to store the translation;
    $sql='insert into ' . $tbname_temp . '( xlcruser, protocolid, studyid, xltest, xlmodify) values ';

    for ($i=0; $i < $source_no; $i++)
    {
        if ($i < $source_no-1){
            $sql .= "('" . $userid . "','" . RUN_DTC . "','" . $jobid . "', '" . $source_text[$i] . "','" . $target_text[$i] . "'),";
        } 
        else{
            $sql .= "('" . $userid . "','" . RUN_DTC . "','" . $jobid . "', '" . $source_text[$i] . "','" . $target_text[$i] . "')";
        }                    
    }

    //execute the sql statement;
    $result = $conn->multi_query($sql);

    if ($result) {
        $res['error'] = false;
        $res['message'] = "Translated successfully";

        //update primary table based on the temp translation results table;
        $sql = "update " . $tbname . " a," . $tbname_temp . " b
        SET a.xlmodtc = '$run_dt',
            a.xlstat = 'API',
            a.xleval = 'API',
            a.xlmodify = b.xlmodify,
            a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid API batch translated')
            where a.xltest=b.xltest and a.xlstat not in ('APPROVED', 'LOCKED') and $where_condition;";

        // $res['sql']= $sql;

        $result = $conn->multi_query($sql);
        
        if ($result) {
            $res['error'] = false;
            $res['message'] = "翻译记录已成功写入数据库！";
            
        }else{
            $res['error'] = true;
            $res['source']= $source_text;
            $res['target']= $target_text;
            $res['sql']= $sql;
            $res['message'] = "翻译记录未写入数据库，请检查后再试！";
        }

        // $res['sql'] = "update " . $tbname . " a," . $tbname_temp . " b
        // SET `a.xlmodtc` = '$run_dt',
        //     `a.xlstat` = 'READY',
        //     `a.xlmodify` = `b.xlmodify`,
        //     `a.xlauditlog` = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid API batch translated')
        // where a.xltest=b.xltest and b.xlcruser= '$userid' and $where_condition;";

    } else{
        $res['error'] = true;
        $res['source']= $source_text;
        $res['target']= $target_text;
        $res['message'] = "翻译临时表错误，请联系管理员！";
    }    
}
else{
    $res['error'] = true;
    $res['debug'] = true;
    $res['source']= $source_text;
    $res['target']= $target_text;
    $res['message'] = "翻译条目结果数不匹配，请检查后再试！";
}
//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>