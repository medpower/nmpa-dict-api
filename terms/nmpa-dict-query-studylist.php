<?php

//retrieve the study list authorized and used for main interface display of current login user;

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

$res = array('error' => false);

$action = 'query';

//Reset the action when applicable;
// if (isset($_POST['action'])) {
// 	$action = $_POST['action'];
// }

// if (isset($_POST['userid']) && !empty($_POST['userid'])) {
// 	$userid = strtoupper($_POST['userid']);
// }
// else{
//     $userid = "PUBLIC";
// }


// $userid=$_POST['userid'];

//Initialize the action as read;

$where_condition="where xlrmfl <> 'Y' and upper(xlcruser)= '$userid' ";

	$result = $conn->query("SELECT distinct protocolid, studyid from " . $tbname . " utf8 " . 
							$where_condition . " ");
	$records = array();
	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}
	$res['sql'] = "SELECT distinct protocolid, studyid from " . $tbname . " utf8 " . 
	$where_condition . " ";
	$res['records'] = $records;

//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>