<?php

include "../config/_init_.php";

cors();
chkJWT();

$res = array('error' => false);

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
	$res['error'] =true;
	$res['message'] = "Error: Database connection established Failed.";
	die("Database connection established Failed.");
} 

//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'filtereditems';
$xltype = "";
$testcode = "";
$status = "";

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}
else{
    $userid = "public";
}


if (isset($_POST['keylist']) && !empty($_POST['keylist']) ) {
	$keylist=urldecode($_POST['keylist']);
    $where_condition= " a.id in (" . $keylist .  ")";
}
else{
    $keylist = "";
    $where_condition= "1 > 0";
}

$res['keylist']=urldecode($_POST['keylist']);
                
$jobid =random_character($length = 8);
$run_dt=RUN_DTC;

// $res['debug'] = "SET session group_concat_max_len = 1024000;
// select group_concat(xltest separator '@@@') as xltest from " . $tbname . 
// " a where upper(xlstat) in ('ACTIVE') and " . $where_condition . ";";

// $keyscope = "global";
// $tbname="_xd_nmpa_" . $keyscope . "_" . $keycategory . "_" . $keytype;
// $tbname_dict="_xd_nmpa_global_" . $keycategory . "_" . $keytype;

$tbname_temp = "_xd_nmpa_study_temp_term";
$result = $conn->query("select distinct a.xltest from " . $tbname . 
                            " a where upper(a.xlstat) not in ('APPROVED', 'LOCKED') and " . $where_condition . ";" );
// $res['sql']="select distinct a.xltest from " . $tbname . " a where upper(a.xlstat) in ('ACTIVE', 'INITIAL') and " . $where_condition . ";";

// function ObjectToArray($o) {
//     if(is_object($o)) $o = get_object_vars($o);
//     if(is_array($o))
//         foreach($o as $k=>$v) $o[$k] = ObjectToArray($v);
//     return $o;
//     }

if($result){
    $res['message'] = "API batch translated successfully";
    // $res['debug'] = "";
    $source = array();
    $query_list="";
    while ($row = $result->fetch_assoc()){
        array_push($source, $row);
    }   
    $res['records-array'] = $source;
   
    //return the query list as a string;
    $dlm="\n";
    $source=array_column($source,'xltest');
    $query_list = implode($dlm, $source);
    $res['query-list'] = $query_list;

    //call translation API;
    
	header('content-type:application:json;charset=utf8');
	header('Access-Control-Allow-Origin:*');
	header('Access-Control-Allow-Methods:POST,GET');
	header('Access-Control-Allow-Headers:x-requested-with,content-type');

    $host = "https://free.niutrans.com";
    $path = "/NiuTransServer/translation";
    $apikey = "eb65376772d4b6ee195a8a8e23a582a1";
	$dictNo = "eed8d9d5d4";
	$memoryNo="71c53902f8";
    // $src_text=urldecode($query_list);
	$from="en";
	$to="zh";
    $durl = $host.$path."?from=". $from . "&src_text=". urlencode($query_list) ."&to=" . $to . "&apikey=".$apikey."&dictNo=".$dictNo."&memoryNo=".$memoryNo;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $durl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $translation_results = curl_exec($ch);
	
    curl_close($ch);

    // $res['query-result'] = $translation_results;
	
	//var_dump($res);
    // $res['translation'] =array_column(explode("@@@", $translation_results),'tgt_text');
    // $res['translation'] =array_column(json_decode($translation_results,true),"tgt_text");
	$json2Array = json_decode($translation_results,true);			
 
    foreach($json2Array as $key=>$val) {
        if ($key == 'tgt_text'){
                       
            $target=explode($dlm,$val);   

            $order=0;         
            $source_no= count($source);

            if ($source_no===1){
                $target_no= count($target);
            }else
            {
                $target_no= count($target)-1;
            }            

            $res['src_no']= $source_no;
            $res['tgt_no']= $target_no;

            if ($source_no==$target_no){
                $res['source']= $source;
                $res['target']= $target;

                //insert into temporary table to store the translation;
                $sql='insert into ' . $tbname_temp . '( xlcruser, protocolid, studyid, xltest, xlmodify) values ';

                for ($i=0; $i < $source_no; $i++)
                {
                    if ($i < $source_no-1){
                        $sql .= "('" . $userid . "','" . RUN_DTC . "','" . $jobid . "', '" . $source[$i] . "','" . $target[$i] . "'),";
                    } 
                    else{
                        $sql .= "('" . $userid . "','" . RUN_DTC . "','" . $jobid . "', '" . $source[$i] . "','" . $target[$i] . "')";
                    }                    
                }

                //execute the sql statement;
                $result = $conn->multi_query($sql);

                if ($result) {
                    $res['error'] = false;
                    $res['message'] = "Translated successfully";

                    //update primary table based on the temp translation results table;
                    $sql = "update " . $tbname . " a," . $tbname_temp . " b
                    SET a.xlmodtc = '$run_dt',
                        a.xlstat = 'API',
                        a.xlmodify = b.xlmodify,
                        a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid API batch translated')
                        where a.xltest=b.xltest and a.xlstat not in ('APPROVED', 'LOCKED') and b.xlcruser= '$userid' and $where_condition;";

                    // $res['sql']= $sql;

                    $result = $conn->multi_query($sql);

                    if ($result) {
                        $res['error'] = false;
                        $res['message'] = "API batch translated successfully";
                        
                    }else{
                        $res['error'] = true;
                        $res['source']= $source;
                        $res['target']= $target;
                        // $res['sql']= $sql;
                        $res['message'] = "API batch translated failed when updates";
                    }

                    // $res['sql'] = "update " . $tbname . " a," . $tbname_temp . " b
                    // SET `a.xlmodtc` = '$run_dt',
                    //     `a.xlstat` = 'READY',
                    //     `a.xlmodify` = `b.xlmodify`,
                    //     `a.xlauditlog` = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid API batch translated')
                    // where a.xltest=b.xltest and b.xlcruser= '$userid' and $where_condition;";

                } else{
                    $res['error'] = true;
                    $res['source']= $source;
                    $res['target']= $target;
                    $res['message'] = "Translated failed when insert temp table";
                }    
               
                // $res['valuelist'] = $sql;                


            }
            else{
                $res['error'] = true;
                $res['debug'] = true;
                $res['source']= $source;
                $res['target']= $target;
                $res['message'] = "Translated failed with unmatched items";
            }

            


            // foreach($target as $value){
                
            //     echo $value . "==>" .$source[$order]  . "<br/>";
                
            //     $order+=1;		

            //     //update the database accordingly;
            //         $result = $conn->query("UPDATE " . $tbname . " 
            //         SET `xlmodify` = '$value',
            //             `xlstat` = 'Machine Translated', `xlmodtc` = '$run_dt'
            //         WHERE `xltest` = '$source[$order]' and `xlrmfl` <> 'Y'
            //         ");
    
            //         if ($result) {
            //             $res['error'] = false;
            //             $res['message'] = "Translated successfully";
            //         } else{
            //             $res['error'] = true;
            //             $res['message'] = "Translated failed";
            //         }    
            // }						
        }		
    }
    
	// header("Content-type: application/json");
	// echo json_encode($translation_results,JSON_UNESCAPED_UNICODE);


}else{
    $res['message'] = "API batch translated failed";
    // $res['debug'] = "";
}


// $records = array();
// while ($row = $result->fetch_assoc()){
//     array_push($records, $row);
// }

// $res['records'] = $records ;

// $result = $conn->multi_query("create temporary table _xd_nmpa_tmp_" . $jobid . " as 
// select distinct xltest from " . $tbname . " where upper(xlstat) in ('ACTIVE') and " . $where_condition . ";" .  
// "update " . $tbname . ", _xd_nmpa_tmp_" . $jobid . "
// SET `xlmodtc` = '$run_dt',
// `xlstat` = 'DELETED'
// ,`xlauditlog` = concat_ws('\\n', ifnull(xlauditlog,'----------'),'$run_dt $userid added DELETE flag')
// where id=seq; 
// drop temporary table _xd_nmpa_tmp_" . $jobid . ";"
// );




// $result = $conn->multi_query("update " . $tbname . " a, " . $tbname_dict . " b " . "  
//             SET a.xlmodtc = '$run_dt',
//                 a.xlmodify = b.xlmodify,
//                 a.xlstat = 'LOCKED',
//                 a.xlauditlog = concat_ws('\\n', ifnull(a.xlauditlog,'----------'),'$run_dt $userid dictionary translated')
//                 where a.xltest=b.xltest and " . $where_condition . ";"
//                 );



//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>