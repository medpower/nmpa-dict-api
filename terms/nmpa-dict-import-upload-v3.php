<?php

include "../config/_init_.php";

cors();
chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 

$res = array('error' => false);

$res['debug'] = "Not available";
			
//Initialize the action as read;
$action = 'upload-csv';

//Reset the action when applicable;
if (isset($_POST['action'])) {
	$action = $_POST['action'];
}

if (isset($_POST['userid']) && !empty($_POST['userid'])) {
	$userid = $_POST['userid'];
}

if (isset($_POST['filename']) && !empty($_POST['filename'])) {
	$filename = $_POST['filename'];
}

// $scope="study";

// $tbname =strtolower("_xd_nmpa_". $scope . "_" . $xlscat ."_" . $xltype);

$targetSubFolder=$userid;
$strFileUploadDate=date("Y-m");
$destination="../upload";

$destination=$destination.DIRECTORY_SEPARATOR.$targetSubFolder.DIRECTORY_SEPARATOR.$strFileUploadDate.DIRECTORY_SEPARATOR.$filename;

$handle = fopen($destination, 'r');
$ext = pathinfo($destination, PATHINFO_EXTENSION);
$filename=pathinfo($destination, PATHINFO_FILENAME);

//set the default values for all records;
$scope='_study_';
$status='INITIAL';
$studyid=strtoupper($filename);
$protocolid=strtoupper($userid);
$xltype='UNASSIGNED';

//derive the information from description accordingly;
if (isset($_POST['description']) && !empty($_POST['description'])){
    $param=explode('|', $_POST['description']);
    if(count($param)===1){
        $protocolid=strtoupper($userid);
        $studyid=$_POST['description'];
    }
    elseif(count($param)===2){        
        $protocolid=$param[0];
        $studyid=$param[1];
    }
    elseif(count($param)===3){
        $protocolid=$param[0];
        $studyid=$param[1];
        $xltype=$param[2];
    }
}
else{
    $protocolid=strtoupper($userid);
    $studyid=strtoupper($filename);
}

if (isset($_POST['removeduplicate']) && !empty($_POST['removeduplicate'])){
    $removeduplicate=$_POST['removeduplicate'];
}
else{
    $removeduplicate=0;
}

//update per request setting;
if($_POST['scope']==1){
    //global default name;
    $scope='_global_';
    //READY for global uploads
    $status='READY';
}
elseif($_POST['scope']==2)
{
    //user selected protocol and study name from database;
    $scope='_study_';
    $status='INITIAL';
    if (isset($_POST['studyid']) && !empty($_POST['studyid'])){
        $studyid=$_POST['studyid'];
    }
    if (isset($_POST['protocolid']) && !empty($_POST['protocolid'])){
        $protocolid=$_POST['protocolid'];
    }    
}
elseif($_POST['scope']==3)
{
    //user customized protocol and study name;
    $scope='_study_';
    $status='INITIAL';
}

if (isset($_POST['comment']) && !empty($_POST['comment'])) {
	$xlcomment = $_POST['comment'];
}
else{
    $xlcomment ='';
}

$xlcrdtc = RUN_DTC;
$xlmodtc = RUN_DTC;
$xlauditlog = RUN_DTC . " " . $userid . " system imported";
$xlcruser=$userid;
$xlmouser='';


//reset the table name to be imported into;

$tbname="_xd_nmpa" . $scope . str_replace('-','_',$_POST['category']);

//remove duplicate or not in current study level;
$uniquevalue=$_POST['removeduplicate'];

$varlabel=['CAT',   'SCAT',   'SUBCAT' , 'CATEGORY', 'SUBCATEGORY', 'TYPE',   'SOURCE', 'TARGET',  'COMMENT',   'ELEMENT',  'NAME',     'LABEL', 'STATUS'];
$varname= ['XLCAT', 'XLSCAT', 'XLSCAT' , 'XLCAT',    'XLSCAT',      'XLTYPE', 'XLTEST', 'XLMODIFY','XLCOMMENT', 'XLTESTCD', 'XLTESTCD', 'XLTEST', 'XLSTAT'];

if(strtoupper($ext)=='CSV'){

    $sql_varlist='';
    $sql_vallist='';
    $rowid=0;
    $sql='';
    $source_order=0;

    while (($data = fgetcsv($handle)) !== false) {
        // 下面这行代码可以解决中文字符乱码问题
        //$data[0] = iconv('gbk', 'utf-8', $data[0]);

        if ($data){

            $seq=0;
            if($rowid==0){               
                foreach($data as $value){                    
                    $idx=array_search($value, $varlabel);      
                    if ($idx){  
                        
                        if ($varname[$idx]==='XLTEST'){
                            $source_order=$seq;
                        }

                        if ($seq==0){
                            $sql_varlist = $varname[$idx] ;
                            }
                        else{
                            $sql_varlist = $sql_varlist . ', ' . $varname[$idx] ;            
                            };                
                            $seq+=1;
                         
                        }

                    }
                    $sql_varlist = "INSERT INTO " . $tbname . 
                    "(protocolid, studyid, " . $sql_varlist . ", xlcruser, xlmouser, xlcrdtc, xlmodtc, xlauditlog) \n";
                    
                    $rowid+=1;
                    continue;
            }
           
            $sql_vallist='';
            // foreach($data as $value){                    
            //     if ($seq==0){
            //         $sql_vallist = "'" . mysqli_real_escape_string($conn,$value) . "'" ;
            //         }
            //     else{
            //         $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$value) . "'";            
            //         };                
            //         $seq+=1;
            //     }

            // 转义特殊字符     

            // $sql = $sql . $sql_varlist . " values ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ); \n " ;
                   
            // $res['sql'] = $sql;

            //only add unique value;
            $res['rf1'] =$removeduplicate;
            $res['rf2'] =$data[$source_order];
            $res['rf3'] =$tbname;
            $res['rf4']= "SELECT * FROM " . $tbname . " utf8 " . 				
            " where xltest = '" . $data[$source_order] . "' and protocolid = '" . $protocolid . "' and studyid = '" . $studyid . "'";

            if ($removeduplicate==1){

                $result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . 				
                            ' where xltest = "' . $data[$source_order] . '" and protocolid = "' . $protocolid . '" and studyid = "' . $studyid . '"');
                            
                $no=$result->num_rows;		

                if ($no==0){

                    foreach($data as $value){                    
                        if ($seq==0){
                            $sql_vallist = "'" . mysqli_real_escape_string($conn,$value) . "'" ;
                            }
                        else{
                            $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$value) . "'";            
                            };                
                            $seq+=1;
                        };

                    // $sql = $sql . $sql_varlist . " values ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ); \n " ;

                    $sql = $sql . $sql_varlist  ;
                    // $i=$i+1;
                }
            }
            // else{

            //     foreach($data as $value){                    
            //         if ($seq==0){
            //             $sql_vallist = "'" . mysqli_real_escape_string($conn,$value) . "'" ;
            //             }
            //         else{
            //             $sql_vallist = $sql_vallist . ', ' . "'" . mysqli_real_escape_string($conn,$value) . "'";            
            //             };                
            //             $seq+=1;
            //         }

            //     $sql = $sql . $sql_varlist . " values ('$protocolid', '$studyid', " . $sql_vallist . "," . "'$xlcruser', '$xlmouser', '$xlcrdtc', '$xlmodtc', '$xlauditlog' ); \n " ;
           
            // }
        
    }

    }

    // $result = $conn->multi_query($sql);
    $result = true;
                    
    if ($result) {
        fclose($handle);		
        $res['message'] = "New record added successfully";
        $res['sql'] = $sql;
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
        //echo $i. "New record added successfully $xlmodify";
    } else{
        fclose($handle);		
        $res['error'] = true;
        $res['sql'] = $sql;
        $res['message'] = "Insert new record failed";
        //echo $i ."Insert new record failed $xlmodify";
        $conn -> close();
        header("Content-type: application/json");
        echo json_encode($res,JSON_UNESCAPED_UNICODE);
        die();
    }
}
// else{

//     // $sql_varlist='';
//     // $sql_vallist='';
//     // $rowid=0;
//     // $sql='';
//     // $source_order=0;

//     // while (($data =  fgetcsv($handle, 0, "\t")) !== false) {

//     //     // 下面这行代码可以解决中文字符乱码问题
  
//     // ｝
// }

$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>