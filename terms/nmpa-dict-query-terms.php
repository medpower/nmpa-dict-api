<?php

include_once "../config/_init_.php";

//use \Firebase\JWT\JWT;

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 


//Initialize the action as read;
$xlcat="";
$xlscat="";
$action = 'read';
$xltype = "";
$testcode = "";
$status = "";

$userid=strtoupper($_POST["userid"]);
$company_domain=strtoupper(explode('@', $_POST["email"])[1]);

$where_condition= " where 1>0 ";

if (isset($_POST['studyid']) && !empty($_POST['studyid'])){
    $where_condition .=" and upper(studyid) = '" . strtoupper($_POST['studyid']) . "'";
}

if (isset($_POST['varname']) && !empty($_POST['varname'])){
    $where_condition .=" and upper(xltestcd) = '" . strtoupper($_POST['varname']) . "'";
}

if (isset($_POST['varlabel']) && !empty($_POST['varlabel'])){
    $where_condition .=" and upper(xltest) = '" . strtoupper($_POST['varlabel']) . "'";
}


if ($keyvalue ==="DELETED"){

	$where_condition .= " and xlrmfl = 'Y' and (upper(xlcruser) = '" . $userid. "' or accesslevel = 1 or (accesslevel = 2 and 
	xlcruser in (select userid from ". JWT_DBUSER . " where  upper(substring_index(email,'@',-1))='" . $company_domain . "')))";

}
elseif ($keyvalue ==="READY"){

	$where_condition .= " and xlrmfl <> 'Y' and (upper(xlcruser) = '" . $userid. "' or  upper(xlmouser) = '" . $userid. "' or  
	find_in_set('" . $userid. "', upper(xlreviewer))) ";

}
elseif ($keyvalue ==="VALUE"){

    $tbname = "_xd_nmpa_study_variable_valuelist";

	$where_condition .= " and xlrmfl <> 'Y' and (upper(xlcruser) = '" . $userid. "' or accesslevel = 1 or (accesslevel = 2 and 
	xlcruser in (select userid from ". JWT_DBUSER . " where  upper(substring_index(email,'@',-1))='" . $company_domain . "')))";
}
else{

	$where_condition .= "and xlrmfl <> 'Y' and (upper(xlcruser) = '" . $userid. "' or accesslevel = 1 or (accesslevel = 2 and 
	xlcruser in (select userid from ". JWT_DBUSER . " where  upper(substring_index(email,'@',-1))='" . $company_domain . "')))";

}

if ($keyvalue ==="SHOW" or $keyvalue ==="LOCKED" or strtolower($keyscope) ==="global"){
	$limitResults='N';
}
else{
	$limitResults='Y';
}

// `id`,
//     `protocolid`,
//     `studyid`,
//     `domain`,
//     `xlcat`,
//     `xlscat`,
//     `xltest`,
//     `xltestcd`,
//     `xlmodify`,
//     `xlstat`,
//     `xlrsn`,
//     `xlver`,
//     `xleval`,
//     `xlorigin`,
//     `xlcruser`,
//     `xlmouser`,
//     `xlcomment`,
//     `xlrmfl`,
//     `xlcrdtc`,
//     `xlmodtc`,
//     `xltype`,
//     `xlauditlog`,
//     `systimestamp`,
//     `xlreviewer`,
//     `xlreader`,
//     `xleditor`,
//     `xlapprover`,
//     `accesslevel`

$varlist ="`id`,
`protocolid`,
`studyid`,
`xlcat`,
`xlscat`,
`xltest`,
`xltestcd`,
`xlmodify`,
`xlorigin`,
`xlstat`,
`xleval`,
`xlcrdtc`,
`xlmodtc`,
`xlcruser`,
`xlmouser`,
`xlauditlog`,
`xltype`,
`xlcomment`,
`accesslevel`";

// $res['sql'] = $where_condition;
//For read operation;
if ($action == 'read') {

	if(urldecode($keyvalue) !="SHOW" && urldecode($keyvalue) !="SHOW ALL" && urldecode($keyvalue) !="VALUE"){				
		$where_condition .= " and upper(" . $keyname . ") like '" . urldecode($keyvalue) . "%'";
	}
	
	if ($limitResults==='Y'){
		$sql = "SELECT " .  $varlist . " FROM " . $tbname . " utf8 " . $where_condition . " order by xltest, xltestcd limit 2000";
	}
	else{
		$sql = "SELECT " .  $varlist . " FROM " . $tbname . " utf8 " . $where_condition . " order by xltest, xltestcd ";
	}
	

	//$result = $conn->query("SELECT * FROM " . $tbname . " utf8 " . $where_condition . " order by xltestcd, xlcrdtc, xlmodtc");
	$result = $conn->query($sql);
	$records = array();

	while ($row = $result->fetch_assoc()){
		array_push($records, $row);
	}	

	//close connection and output json object;
	$conn -> close();
	
	//$payload = decode($userToken, JWT_KEY); 
	//$decoded_array = (array) $decoded;
	//$jwt=encode($payload, JWT_KEY);
	
	//$res['now'] = $payload['exp'];
	//$res['jwt'] = chkJWT(userToken);
	$res['debug'] = $sql;//decode($userToken, JWT_KEY);
	$res['message'] = "Query successfully";
	$res['records'] = $records;
	$res['hdr']=$headers['Authorization'];
}

header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>