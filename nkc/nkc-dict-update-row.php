<?php

include "../config/nkc/_init_.php";

cors();

chkJWT();

// Create connection
$conn = new mysqli(JWT_SERVERNAME, JWT_USERID, JWT_PASSWORD, JWT_DBNAME);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
	die("Database connection established Failed..");
} 
$res = array('error' => false);

$column_type_sql="SELECT column_name,data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'ndp' AND TABLE_NAME = '$tbname';";
$column_type_result = $conn->query($column_type_sql);

$columns = array();
$types = array();

while ($row = $column_type_result->fetch_assoc()){
	array_push($columns, $row["column_name"]);
	array_push($types, $row["data_type"]);
}	

//Initialize the action as read;
//Only change the following section parameters per specific project;
$action = $_POST['action'];
$debug = 'N';

$strUpdate="";
$varlist="";
$strAuditlog="";

if (isset($_POST['debug']) && !empty($_POST['debug'])) {
	$debug=urldecode($_POST['debug']);
}

if (isset($_POST['varlist']) && !empty($_POST['varlist'])) {
	$updated_varlist=explode(' ', urldecode($_POST['varlist']));
}

if (isset($_POST['auditlog']) && !empty($_POST['auditlog'])) {
	$updated_auditlog=explode('@@@@', urldecode($_POST['auditlog']));
}

$key_name = urldecode($_POST['key_name']);
$key_value = urldecode($_POST['key_value']);

$where_condition="where 1>0 ";

$key_type= $types[array_search($key_name, $columns)];

if ($key_type=='char' || $key_type=='text' || $key_type =='varchar'){
	$where_condition = $where_condition . " and " . $key_name . " = '" . $key_value . "'";    
}
else{
	$where_condition = $where_condition . " and " . $key_name . " = " . $key_value;
}

//Please do not change the codes below; if you are not sure how it works;
//for cell update operation;
if ($action == 'row-update-general') {

	//construct the update statement per the varlist value;

	$seq=0;
	foreach ( $updated_varlist as $var) { 
		//determine the field type;

		$type= $types[array_search($var, $columns)];

		if ($type=='char' || $type=='text' || $type =='varchar'){
			$vartype='C';
		}
		else{
			$vartype='N';
		}

		if ($seq==0){
			if ($vartype=='C'){
				$strUpdate = $var . '= "' . addslashes(urldecode($_POST[$var])) . '"';
			}
			else{
				$strUpdate = $var . '= ' . addslashes(urldecode($_POST[$var])) ;
			}
		}
		else{
			if ($vartype=='C'){
				$strUpdate =$strUpdate . "," . $var . '= "' . addslashes(urldecode($_POST[$var])) . '"';
			}
			else{
				$strUpdate =$strUpdate . "," . $var . '= ' . addslashes(urldecode($_POST[$var])) ;
			}
		
		}
		$seq+=1;
	} 

	$seq=0;
	foreach ( $updated_auditlog as $log) { 
		if ($seq==0){
			$strAuditlog = RUN_DTC . " " . $_POST['userid'] . ' updated ' . addslashes($log);
		}
		else{
			$strAuditlog =$strAuditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated ' . addslashes($log);
		}
		$seq+=1;
	} 

	$strUpdate =$strUpdate . ", auditlog = concat_ws('\\n', ifnull(auditlog,''),'$strAuditlog') ";

	$modtc  = RUN_DTC;
	$userid=$_POST['userid'];

	$sql="UPDATE " . $tbname . " 
	SET " . $strUpdate . ", modtc = '$modtc', mouserid = '$userid' " . $where_condition;

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "Row saved";

        if ($debug=='Y'){
            $res['sql']=$sql;
			$res['columns'] = $columns;
			$res['types'] = $types;
        }
				
	} else{
		$res['error'] = true;
		$res['message'] = "Row saved failed";
		$res['debug'] = "Not applicable";
		// $res['auditlog']=$xlauditlog;
        if ($debug=='Y'){
            $res['sql']=$sql;
			$res['columns'] = $columns;
			$res['types'] = $types;
        }
	}
}
elseif($action == 'row-update-budget') {

	//construct the update statement per the varlist value;

	$seq=0;
	foreach ( $updated_varlist as $var) { 
		//determine the field type;

		$type= $types[array_search($var, $columns)];

		if ($type=='char' || $type=='text' || $type =='varchar'){
			$vartype='C';
		}
		else{
			$vartype='N';
		}

		if ($seq==0){
			if ($vartype=='C'){
				$strUpdate = $var . '= "' . addslashes(urldecode($_POST[$var])) . '"';
			}
			else{
				$strUpdate = $var . '= ' . addslashes(urldecode($_POST[$var])) ;
			}
		}
		else{
			if ($vartype=='C'){
				$strUpdate =$strUpdate . "," . $var . '= "' . addslashes(urldecode($_POST[$var])) . '"';
			}
			else{
				$strUpdate =$strUpdate . "," . $var . '= ' . addslashes(urldecode($_POST[$var])) ;
			}
		
		}
		$seq+=1;
	} 

	$seq=0;
	foreach ( $updated_auditlog as $log) { 
		if ($seq==0){
			$strAuditlog = RUN_DTC . " " . $_POST['userid'] . ' updated ' . addslashes($log);
		}
		else{
			$strAuditlog =$strAuditlog . "\n" . RUN_DTC . " " . $_POST['userid'] . ' updated ' . addslashes($log);
		}
		$seq+=1;
	} 

	$strUpdate =$strUpdate . ", auditlog = concat_ws('\\n', ifnull(auditlog,''),'$strAuditlog') ";

	$modtc  = RUN_DTC;
	$userid=$_POST['userid'];

	$sql="UPDATE " . $tbname . " 
	SET " . $strUpdate . ", modtc = '$modtc', mouserid = '$userid' " . $where_condition;

	$result = $conn->query($sql);
	
	if ($result) {
		$res['message'] = "Row saved";

        if ($debug=='Y'){
            $res['sql']=$sql;
			$res['columns'] = $columns;
			$res['type'] = $type;
			$res['vartype'] = $vartype;
        }
				
	} else{
		$res['error'] = true;
		$res['message'] = "Row saved failed";
		$res['debug'] = "Not applicable";
		// $res['auditlog']=$xlauditlog;
        if ($debug=='Y'){
            $res['sql']=$sql;
			$res['columns'] = $columns;
			$res['types'] = $types;
        }
	}
}

$res['debug'] = "Not applicable";


//close connection and output json object;
$conn -> close();
header("Content-type: application/json");
echo json_encode($res,JSON_UNESCAPED_UNICODE);
die();

?>